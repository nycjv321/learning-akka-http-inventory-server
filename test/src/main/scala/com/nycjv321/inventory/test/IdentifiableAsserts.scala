package com.nycjv321.inventory.test

import com.nycjv321.inventory.Identifiable
import org.scalatest.Assertions

trait IdentifiableAsserts extends Assertions {
  def assertIdentifiable(identifiable: Identifiable, description: String = "expected entity to have id"): Unit = {
    identifiable.id match {
      case Some(id) => assert(id > 0)
      case None => fail(description)
    }
  }
}
