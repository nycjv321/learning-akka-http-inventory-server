package com.nycjv321.inventory.test

import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.google.inject.Injector
import org.scalatest.{ Matchers, WordSpec }

trait RouteSpec extends WordSpec with Matchers with ScalatestRouteTest with IdentifiableAsserts {
  val injector: Injector
  implicit val routeTest: ScalatestRouteTest = this
}
