package com.nycjv321.database

import doobie.implicits._
import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}
import PingQueries._

class Ping(stoner: FutureDatabaseQueries[Int]) {

  @Inject
  def this(
      stoner: DatabaseQueries
  )(implicit executionContext: ExecutionContext) {
    this(new FutureDatabaseQueries[Pong](stoner))
  }

  def pong: Future[Option[Int]] = stoner.option(PingQueries.ping)
}

object PingQueries {
  type Pong = Int

  def ping = sql"select 1;"
}
