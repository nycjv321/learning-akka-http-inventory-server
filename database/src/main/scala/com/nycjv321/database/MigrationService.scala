package com.nycjv321.database

import org.flywaydb.core.Flyway
import org.flywaydb.core.api.FlywayException

object MigrationService {

  def migrate()(
      implicit
      configuration: DatabaseConfiguration
  ): Either[FlywayException, Int] = {
    val flyway = Flyway.configure
      .dataSource(configuration.url, configuration.user, configuration.password)
      .locations("migrations")
      .load
    try {
      val migrationsExecuted = flyway.migrate()
      Right(migrationsExecuted)
    } catch {
      case (e: FlywayException) => Left(e)
    }
  }
}
