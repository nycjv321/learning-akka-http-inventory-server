package com.nycjv321.database

import akka.actor.ActorSystem
import cats.effect._
import doobie.implicits._
import doobie.{ConnectionIO, _}
import javax.inject.Inject

import scala.annotation.tailrec
import scala.concurrent.ExecutionContext

class DatabaseQueries @Inject() (
    databaseManager: DatabaseManager,
    system: ActorSystem
)(
    implicit
    val executionContext: ExecutionContext
) {
  val transactor = databaseManager.transactor

  def update[B](fragment: Fragment): IO[Either[Throwable, Int]] = {
    updateMany(fragment)
  }

  /**
    * This is not suitable for batch/bulk inserts!
    * @param fragments
    * @return
    */
  def updateMany(fragments: Fragment*): IO[Either[Throwable, Int]] = {
    fragments.headOption match {
      case Some(value) =>
        transactor.use { xa =>
          merge(fragments.tail, value.update.run).transact(xa).attempt
        }
      case None => IO(Right(0))
    }
  }

  implicit class FragmentAddition(fragment: Fragment) {
    def +(connectionIO: ConnectionIO[Int]): ConnectionIO[Int] = {
      for {
        a <- fragment.update.run
        b <- connectionIO
      } yield (a + b)
    }

    def +++[B](connectionIO: ConnectionIO[B]): ConnectionIO[B] = {
      for {
        _ <- fragment.update.run
        b <- connectionIO
      } yield {
        b
      }
    }
  }

  @tailrec
  private def merge(
      fragments: Seq[Fragment],
      opt: ConnectionIO[Int]
  ): ConnectionIO[Int] = {
    fragments match {
      case Nil       => opt
      case x +: Nil  => x + opt
      case (x +: xs) => merge(xs, x + opt)
    }
  }

  @tailrec
  private def mergeType[B](
      fragments: Seq[Fragment],
      opt: ConnectionIO[B]
  ): ConnectionIO[B] = {
    fragments match {
      case Nil      => opt
      case x +: Nil => x +++ opt
      case x +: xs  => mergeType(xs, x +++ opt)
    }
  }

  def insertAndReturn[B: Read](
      columns: String*
  )(fragments: Fragment*): IO[Either[Throwable, B]] = {
    fragments.headOption match {
      case Some(value) =>
        transactor.use { xa =>
          mergeType(
            fragments.tail,
            value.update.withUniqueGeneratedKeys[B](columns: _*)
          ).transact(xa).attempt
        }
      case None => IO(Left(new RuntimeException("query failed :(")))
    }
  }

  def query[B](fragment: Fragment)(
      f: (Fragment, Transactor[IO]) => ConnectionIO[B]
  ): IO[Either[Throwable, B]] = {
    transactor.use { xa =>
      // Construct and run your server here!
      for {
        n <- f(fragment, xa).transact(xa).attempt
      } yield {
        n
      }
    }
  }

  def transact[B](
      fragment: Fragment
  )(f: (Fragment, Transactor[IO]) => ConnectionIO[B]): IO[B] = {
    transactor.use { xa =>
      // Construct and run your server here!
      for {
        n <- f(fragment, xa).transact(xa)
      } yield {
        n
      }
    }
  }

}
