package com.nycjv321.database

import cats.implicits._
import com.nycjv321.inventory.{Id, UUIDIdentifier}
import com.typesafe.scalalogging.LazyLogging
import doobie.implicits._
import doobie.postgres._
import doobie.postgres.implicits._
import doobie.{Fragment, _}
import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}

class FutureDatabaseQueries[A: Read: Write] @Inject() (
    val stoner: DatabaseQueries
)(
    implicit
    val executionContext: ExecutionContext
) extends LazyLogging {

  def delete(
      fragment: Fragment,
      constraints: Constraints
  ): Future[Either[Throwable, Int]] = {
    stoner.update[Int](fragment ++ constraints.to_f).unsafeToFuture()
  }

  def deleteAll(name: Fragment): Future[Either[Throwable, Int]] = {
    stoner.update(fr"truncate " ++ name).unsafeToFuture()
  }

  def option(fragment: Fragment): Future[Option[A]] = {
    stoner
      .query[Option[A]](fragment) { (_, _) =>
        fragment.query[A].option
      }
      .unsafeToFuture()
      .map {
        case Left(error) =>
          logger.error(s"$error")
          None
        case Right(result) => result
      }
  }

  def one[B: Read](fragment: Fragment): Future[B] = {
    stoner
      .transact[B](fragment) { (_, _) =>
        fragment.query[B].unique
      }
      .unsafeToFuture()
  }

  def list(fragment: Fragment): Future[List[A]] = {
    stoner
      .query[List[A]](fragment) { (_, _) =>
        fragment.query[A].accumulate
      }
      .unsafeToFuture()
      .map {
        case Left(error) =>
          logger.error(s"$error")
          List.empty[A]
        case Right(r) => r
      }
  }

  def insert(fragment: Fragment*): Future[Either[Throwable, Int]] = {
    stoner.updateMany(fragment: _*).unsafeToFuture()
  }

  def update(fragment: Fragment*): Future[Either[Throwable, Int]] = {
    stoner.updateMany(fragment: _*).unsafeToFuture()
  }

  def insertAndReturn(fragment: Fragment*): Future[Either[Throwable, A]] = {
    stoner.insertAndReturn[A]("*")(fragment: _*).unsafeToFuture()
  }

  def insertAndReturnID(fragment: Fragment*): Future[Either[Throwable, Id]] = {
    stoner.insertAndReturn[Id]("*")(fragment: _*).unsafeToFuture()
  }

  def insertAndReturnUUID(
      fragment: Fragment*
  ): Future[Either[Throwable, UUIDIdentifier]] = {
    stoner.insertAndReturn[UUIDIdentifier]("*")(fragment: _*).unsafeToFuture()
  }

}
