package com.nycjv321.database

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

trait Migratable extends LazyLogging {
  def migrateOnSuccess(ping: Ping)(
      implicit
      system: ActorSystem,
      configuration: DatabaseConfiguration,
      executionContext: ExecutionContext
  ): Unit = {
    val pong = ping.pong
    while (!pong.isCompleted) {
      Thread.sleep(100)
    }
    pong.value match {
      case Some(Failure(error)) =>
        logger.error(s"Unable to connect to ${configuration.url}, $error")
        system.terminate()
      case Some(Success(Some(_))) =>
        logger.info(s"Connected to ${configuration.url}")
        MigrationService.migrate() match {
          case Left(error) =>
            logger.error(s"$error")
            system.terminate()
          case Right(count) if count > 0 =>
            logger.info(s"Executed $count migrations")
          case Right(_) => logger.info(s"No new migrations executed")
        }
      case _ =>
        logger.error(s"timed out connecting to ${configuration.url}")
        system.terminate()
    }
  }
}
