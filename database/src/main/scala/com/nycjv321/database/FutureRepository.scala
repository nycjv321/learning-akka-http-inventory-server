package com.nycjv321.database

import doobie._
import doobie.implicits._
import doobie.util.fragment.Fragment

import scala.concurrent.{ExecutionContext, Future}
import com.nycjv321.database.Querys.{column, where}

abstract class FutureRepository[A](stoner: FutureDatabaseQueries[A])(
    implicit
    val executionContext: ExecutionContext
) {

  implicit def alias: Fragment
  def tableName: String

  def map: Future[List[A]] = stoner.list(sql"select * from " ++ alias)

  def exists(constraints: Constraint*): Future[Boolean] = {
    stoner.one[Boolean](existsQuery(constraints: _*))
  }

  def find(constraints: Constraint*): Future[Option[A]] = {
    stoner.option(findQuery(constraints: _*))
  }

  def filter(constraints: Constraint*): Future[List[A]] =
    stoner.list(selectFrom ++ Constraints(constraints.toSeq).to_f)

  def list(): Future[List[A]] =
    stoner.list(selectFrom)

  def findById(id: Int): Future[Option[A]] = {
    find(where(column(s"$tableName.id") === (id)))
  }

  def deleteById(id: Int): Future[Either[Throwable, Int]] = {
    delete(where(column(s"$tableName.id") === (id)))
  }

  def delete(constraints: Constraint*): Future[Either[Throwable, Int]] =
    stoner.delete(sql"delete from " ++ alias, Constraints(constraints))

  def deleteAll: Future[Either[Throwable, Int]] = {
    stoner.deleteAll(alias)
  }

  def insert(fragment: Fragment*): Future[Either[Throwable, Int]] = {
    stoner.insert(fragment: _*)
  }

  def selectFrom(implicit alias: Fragment): Fragment =
    sql"select * from " ++ alias

  def findQuery(constraints: Constraint*)(implicit alias: Fragment) =
    selectFrom ++ new Constraints(constraints.toSeq).to_f
  def existsQuery(
      constraints: Constraint*
  )(implicit alias: Fragment): Fragment =
    sql"select count(*) > 0 from " ++ alias ++ new Constraints(
      constraints.toSeq
    ).to_f

}

case class Constraints(constraints: Seq[Constraint]) {
  def to_f = constraints.map(_.fragment).foldLeft(fr"")(_ ++ _)
}

case class Constraint(fragment: Fragment) {
  def to_f = fragment
}

case class ConstraintBuilder(field: String) {
  def like[A: Put](value: A): Constraint = {
    Constraint(Fragment.const(field) ++ fr"like $value")
  }

  def ===[A: Put](value: A): Constraint = {
    Constraint(Fragment.const(field) ++ fr"= $value")
  }

}

object Querys {

  def column(column: String): ConstraintBuilder = ConstraintBuilder(column)

  def where(a: Constraint*): Constraint = {
    Constraint(a.foldLeft(fr"where")((a, b) => a ++ b.fragment))
  }

  def and(a: Constraint*): Constraint = {
    Constraint(a.foldLeft(fr"and")((a, b) => a ++ b.fragment))
  }

  def or(a: Constraint*): Constraint = {
    Constraint(a.foldLeft(fr"or")((a, b) => a ++ b.fragment))
  }

}
