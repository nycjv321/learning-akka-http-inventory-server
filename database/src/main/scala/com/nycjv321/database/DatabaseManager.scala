package com.nycjv321.database

import cats.effect._
import doobie._
import doobie.hikari._
import javax.inject.Inject
import pureconfig.error.ConfigReaderFailures
import pureconfig.generic.auto._
import scala.concurrent.ExecutionContext

case class DatabaseConfiguration(
    user: String,
    password: String,
    host: Option[String],
    port: Option[Int],
    database: Option[String]
) {
  val url =
    s"jdbc:postgresql://${host.getOrElse("localhost")}:${port.getOrElse(5432)}/${database.getOrElse("postgres")}"
}

object DatabaseConfiguration {

  val read: Either[ConfigReaderFailures, DatabaseConfiguration] = {
    pureconfig.loadConfig[DatabaseConfiguration]("database")
  }

  val error: DatabaseConfiguration =
    DatabaseConfiguration("", "", None, None, None)
}

class DatabaseManager @Inject() (databaseConfiguration: DatabaseConfiguration)(
    implicit
    val executionContext: ExecutionContext
) {

  implicit val contextShift: ContextShift[IO] =
    IO.contextShift(executionContext)

  val transactor: Resource[IO, HikariTransactor[IO]] =
    for {
      ce <- ExecutionContexts.fixedThreadPool[IO](32) // our connect EC
      be <- Blocker[IO]                               // our blocking EC
      xa <- HikariTransactor.newHikariTransactor[IO](
        "org.postgresql.Driver",        // driver classname
        databaseConfiguration.url,      // connect URL
        databaseConfiguration.user,     // username
        databaseConfiguration.password, // password
        ce,                             // await connection here
        be                              // execute JDBC operations here
      )
    } yield xa

  def execute[M, T](f: HikariTransactor[IO] => IO[T]) = {
    transactor.use(f)
  }

}
