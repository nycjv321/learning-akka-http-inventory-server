package com.nycjv321.inventory

import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{StatusCode, StatusCodes}
import akka.http.scaladsl.server.Directives.{
  as,
  entity,
  onSuccess,
  respondWithHeaders
}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.http.scaladsl.unmarshalling.FromRequestUnmarshaller

import scala.concurrent.Future

trait SimplifiedRoutes {
  def config: RouteConfig = RouteConfig(AllowControl())

  def getAll[A](
      operation: => Future[A]
  )(implicit f: ((StatusCode, A)) => ToResponseMarshallable): Route = {
    MethodDirectives.get {
      onSuccess(operation) { resolvedMovies =>
        respondWithHeaders(
          RawHeader(
            "Access-Control-Allow-Origin",
            config.allowControl.allowOrigin
          )
        ) {
          complete((StatusCodes.OK, resolvedMovies))
        }
      }
    }
  }

  def options[A](
      methods: Seq[String] = Seq("OPTIONS", "HEAD", "POST", "GET", "DELETE")
  )(implicit f: ((StatusCode, A)) => ToResponseMarshallable): Route = {
    MethodDirectives.options {
      respondWithHeaders(
        RawHeader(
          "Access-Control-Allow-Origin",
          config.allowControl.allowOrigin
        ),
        RawHeader("Allow", methods.mkString(",")),
        RawHeader("Access-Control-Allow-Methods", methods.mkString(",")),
        RawHeader("Access-Control-Allow-Headers", "Content-Type")
      ) {
        complete((StatusCodes.NoContent))
      }
    }
  }

  def post[A](
      operation: A => Future[Either[Message, A]],
      status: StatusCode = StatusCodes.Created
  )(
      implicit
      f: ((StatusCode, A)) => ToResponseMarshallable,
      um: FromRequestUnmarshaller[A],
      m: ((StatusCode, Message)) => ToResponseMarshallable
  ): Route = {
    MethodDirectives.post {
      entity(as[A]) { entity =>
        onSuccess(operation(entity)) {
          case Left(message) => {
            respondWithHeaders(
              RawHeader(
                "Access-Control-Allow-Origin",
                config.allowControl.allowOrigin
              )
            ) {
              complete((StatusCodes.BadRequest, message))
            }
          }
          case Right(value) => {
            respondWithHeaders(RawHeader("Access-Control-Allow-Origin", "*")) {
              complete((status, value))
            }
          }
        }
      }
    }
  }

  def delete[A](id: Int, operation: => Future[Either[Message, Int]])(
      implicit
      m: ((StatusCode, Message)) => ToResponseMarshallable
  ): Route = {
    MethodDirectives.delete {
      respondWithHeaders(
        RawHeader(
          "Access-Control-Allow-Origin",
          config.allowControl.allowOrigin
        )
      ) {

        onSuccess(operation) {
          case Left(value) => complete((StatusCodes.BadRequest, value))
          case Right(_) =>
            complete((StatusCodes.OK, Message(s"entity ($id) was deleted")))
        }
      }
    }
  }

  def get[A: Manifest](id: Int, operation: => Future[Option[A]])(
      implicit
      f: ((StatusCode, A)) => ToResponseMarshallable,
      m: ((StatusCode, Message)) => ToResponseMarshallable,
      um: FromRequestUnmarshaller[A]
  ): Route = {
    MethodDirectives.get {
      respondWithHeaders(RawHeader("Access-Control-Allow-Origin", "*")) {
        onSuccess(operation) {
          case Some(entity) => complete((StatusCodes.OK, entity))
          case None => {
            complete(
              (
                StatusCodes.NotFound,
                new NotFoundError[A](id).asInstanceOf[Message]
              )
            )
          }
        }
      }
    }
  }
}
