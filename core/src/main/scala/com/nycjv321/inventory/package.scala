package com.nycjv321

import java.util.UUID

import cats.data.EitherT
import pureconfig.error.ConfigReaderFailures
import pureconfig.generic.auto._

import scala.concurrent.Future
import scala.reflect.ClassTag

package object inventory {

  type Id             = Option[Int]
  type UUIDIdentifier = Option[UUID]
  val MissingId: Id = None

  implicit def futureEitherTToFutureEither[A, B](
      eitherT: EitherT[Future, A, B]
  ): Future[Either[A, B]] =
    eitherT.value

  trait Identifiable {
    def id: Id
  }

  trait IdentifiableUUID {
    def id: UUIDIdentifier
  }

  trait Versioned {
    def version: Version
  }

  type Version = Int
  val BaselineVersion: Version = 0

  class Message(protected val message: String) {
    override def toString: String = message
  }

  object Message {
    def apply(message: String): Message = new Message(message)
  }

  case class AddError(override val message: String) extends Message(message) {
    def this(identifiable: Identifiable) {
      this(
        message =
          s"unable to add ${identifiable.getClass.getSimpleName} (${identifiable.id})"
      )
    }
  }

  case class NotFoundError[T] private (override val message: String)
      extends Message(message) {
    def this(id: Any)(implicit tag: ClassTag[T]) {
      this(message = s"${tag.runtimeClass.getSimpleName} ($id) doesn't exist")
    }
  }

  case class ConflictError[T] private (override val message: String)(
      implicit
      tag: ClassTag[T]
  ) extends Message(message) {

    def this()(implicit tag: ClassTag[T]) {
      this(s"${tag.getClass.getSimpleName}  already exists")
    }

  }

  case class Server(port: ServerPortNumber = -1, host: String = "localhost")

  type ServerPortNumber = Int

  implicit class ImplicitServerPortNumber(portNumber: ServerPortNumber) {
    def getOrElse(f: => Int): Int = {
      if (portNumber < 0) {
        f
      } else {
        portNumber
      }
    }
  }

  case class HttpConfiguration(server: Server)

  object HttpConfiguration {

    val read: Either[ConfigReaderFailures, HttpConfiguration] = {
      pureconfig.loadConfig[HttpConfiguration]("http")
    }

    val error: HttpConfiguration = HttpConfiguration(Server(0))
  }

  case class RouteConfig(allowControl: AllowControl)

  case class AllowControl(allowOrigin: String = "*")

}
