package com.nycjv321.inventory

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

trait InventoryServer extends LazyLogging {
  def bind(
      serverBinding: Future[Http.ServerBinding]
  )(implicit system: ActorSystem, executionContext: ExecutionContext): Unit = {
    serverBinding.onComplete {
      case Success(bound) =>
        logger.info(
          s"Server online at http://${bound.localAddress.getHostString}:${bound.localAddress.getPort}/"
        )
      case Failure(e) =>
        logger.error(s"Server could not start! $e")
        system.terminate()
    }
  }

}
