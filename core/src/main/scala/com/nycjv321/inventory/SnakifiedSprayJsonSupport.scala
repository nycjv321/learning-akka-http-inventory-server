package com.nycjv321.inventory

import spray.json._

/**
  * A custom version of the Spray DefaultJsonProtocol with a modified field naming strategy
  */
trait SnakifiedSprayJsonSupport extends DefaultJsonProtocol {
  import reflect._
  import SnakifiedSprayJsonSupport._

  /**
    * This is the most important piece of code in this object!
    * It overrides the default naming scheme used by spray-json and replaces it with a scheme that turns camelcased
    * names into snakified names (i.e. using underscores as word separators).
    */
  override protected def extractFieldNames(classTag: ClassTag[_]) = {
    import java.util.Locale

    def snakify(name: String): String =
      PASS2
        .replaceAllIn(PASS1.replaceAllIn(name, REPLACEMENT), REPLACEMENT)
        .toLowerCase(Locale.US)

    super.extractFieldNames(classTag).map(snakify)
  }

  implicit object MessageFormat extends RootJsonFormat[Message] {
    def write(c: Message): JsObject = {
      JsObject(("message", JsString.apply(c.toString)))
    }

    override def read(json: JsValue): Message = {
      new Message(json.asJsObject.getFields("message").head.convertTo[String])
    }
  }

}

object SnakifiedSprayJsonSupport extends SnakifiedSprayJsonSupport {
  private val PASS1       = """([A-Z]+)([A-Z][a-z])""".r
  private val PASS2       = """([a-z\d])([A-Z])""".r
  private val REPLACEMENT = "$1_$2"

}
