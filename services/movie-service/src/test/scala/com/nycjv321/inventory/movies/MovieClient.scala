package com.nycjv321.inventory.movies

import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import JsonSupport._
import com.nycjv321.inventory.{Id, Message}
import org.scalatest.concurrent.ScalaFutures

class MovieClient(routes: Route)(implicit val routeTest: ScalatestRouteTest)
    extends ScalaFutures {
  import routeTest._

  def getMovies(
      statusCode: StatusCode = StatusCodes.OK,
      contentType: ContentType = ContentTypes.`application/json`
  ): Movies = {
    val request = routeTest.Get(uri = "/movies")
    request ~> routes ~> check {
      verifyHeaders(statusCode, contentType)
      entityAs[Movies]
    }
  }

  def get(
      id: Id,
      statusCode: StatusCode = StatusCodes.OK,
      contentType: ContentType = ContentTypes.`application/json`
  ): Either[Message, Movie] = {
    val request = routeTest.Get(
      uri = s"/movies/${id.getOrElse(failTest("movie did not have id"))}"
    )
    request ~> routes ~> check {
      verifyHeaders(statusCode, contentType)
      entityAs[Either[Message, Movie]]
    }
  }

  def add(
      movie: Movie,
      statusCode: StatusCode = StatusCodes.Created,
      contentType: ContentType = ContentTypes.`application/json`
  ): Movie = {
    val movieEntity = Marshal(movie).to[MessageEntity].futureValue // futureValue is from ScalaFutures
    val request     = Post("/movies").withEntity(movieEntity)

    request ~> routes ~> check {
      verifyHeaders(statusCode, contentType)
      entityAs[Movie]
    }
  }

  def delete(
      id: Id,
      statusCode: StatusCode = StatusCodes.Created,
      contentType: ContentType = ContentTypes.`application/json`
  ): Message = {
    val request = Delete(
      s"/movies/${id.getOrElse(failTest("plant did not have id  "))}"
    )

    request ~> routes ~> check {
      verifyHeaders(statusCode, contentType)
      entityAs[Message]
    }
  }

  private def verifyHeaders(
      statusCode: StatusCode = StatusCodes.OK,
      contentType: ContentType = ContentTypes.`application/json`
  )(implicit routeTest: ScalatestRouteTest): Unit = {
    assert(
      routeTest.status == statusCode,
      s"${routeTest.status} == $statusCode, ${entityAs[String]}"
    )
    assert(
      routeTest.contentType == contentType,
      s"${routeTest.status} == $statusCode, ${entityAs[String]}"
    )
  }

}

object MovieClient {
  def apply(routes: Route)(
      implicit routeTest: ScalatestRouteTest
  ): MovieClient = new MovieClient(routes)
}
