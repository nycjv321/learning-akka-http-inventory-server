package com.nycjv321.inventory.movies

import java.time.LocalDate

import akka.actor.Props
import akka.http.scaladsl.model._
import com.google.inject.Injector
import com.nycjv321.database.{DatabaseConfiguration, Migratable, Ping}
import com.nycjv321.inventory.test.RouteSpec
import com.nycjv321.inventory.{Message, Modules, NotFoundError}

class MoviesRouteSpec extends RouteSpec with Migratable {
  override val injector: Injector = Modules.asInjectable
  private val movieRepository     = injector.getInstance(classOf[MovieRepository])
  private val movieService        = injector.getInstance(classOf[MovieService])

  private implicit val databaseConfiguration =
    injector.getInstance(classOf[DatabaseConfiguration])

  lazy val routes = new MoviesRoute(
    system,
    system.actorOf(
      Props(new MovieRegistry(system, movieService)),
      name = "movie-registry-actor"
    )
  ).routes
  migrateOnSuccess(injector.getInstance(classOf[Ping]))

  var movie: Movie = _

  "MoviesRoutes" should {
    // make this synchronous
    for {
      result <- movieRepository.deleteAll
    } yield {
      result match {
        case Left(error) => fail(error)
        case Right(_)    =>
      }
    }
    val movies = MovieClient(routes)
    "return no movies if nil" in {
      // note that there's no need for the host part in the uri:
      assert(movies.getMovies().isEmpty)
    }
    "be able to add movies" in {
      movie = movies.add(
        Movie(title = "Kapi", releaseDate = LocalDate.of(2019, 7, 7))
      )
      assertIdentifiable(movie)
      movie.id match {
        case Some(id) => assert(id > 0)
        case None     => fail("expected entity to have id")
      }
    }
    "be able to retrieve an individual movie" in {
      movies.get(movie.id) match {
        case Left(message) =>
          fail(s"did not expect to receive an error: $message")
        case Right(resolvedMovie) => resolvedMovie should ===(movie)
      }
    }
    "not be able to retrieve non-existent movies" in {
      val queriedMovie =
        movies.get(Some(24213), statusCode = StatusCodes.NotFound)
      queriedMovie match {
        case Left(message: Message) => {
          message.toString should ===(new NotFoundError[Movie](24213).message)
        }
        case Right(_) => fail("did not expect to get back a movie")
      }
    }
    "be able to delete movies" in {
      movies.delete(movie.id, statusCode = StatusCodes.OK)
    }
  }
}
