create table if not exists movies (
    id           serial   primary key,
    title        varchar(300),
    release_date timestamp,
    description  text,
    created      timestamp with time zone default now() not null,
    modified     timestamp,
    constraint uniq_movie unique (title, release_date)
);

create function update_modified_column() returns trigger
    language plpgsql
as
$$
BEGIN
    NEW.modified = now();
    RETURN NEW;
END;
$$;

CREATE TRIGGER update_movie_modified_time BEFORE UPDATE ON movies FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();