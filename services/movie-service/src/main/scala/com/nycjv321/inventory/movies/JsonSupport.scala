package com.nycjv321.inventory.movies

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.nycjv321.inventory.{Message, SnakifiedSprayJsonSupport}
import spray.json._

import scala.util.Try

object JsonSupport extends SprayJsonSupport with SnakifiedSprayJsonSupport {

  implicit val localDateFormat = new JsonFormat[LocalDate] {
    override def write(obj: LocalDate): JsValue =
      JsString(formatter.format(obj))

    override def read(json: JsValue): LocalDate = {
      json match {
        case JsString(lDString) =>
          Try(LocalDate.parse(lDString, formatter))
            .getOrElse(deserializationError(deserializationErrorMessage))
        case _ => deserializationError(deserializationErrorMessage)
      }
    }

    private val formatter = DateTimeFormatter.ISO_LOCAL_DATE
    private val deserializationErrorMessage =
      s"Expected date time in ISO offset date time format ex. ${LocalDate.now().format(formatter)}"
  }

  implicit val userJsonFormat: RootJsonFormat[Movie]   = jsonFormat4(Movie)
  implicit val usersJsonFormat: RootJsonFormat[Movies] = jsonFormat1(Movies)

}
