package com.nycjv321.inventory.movies

import cats.data.EitherT
import cats.data.EitherT.fromOptionF
import cats.implicits._
import com.nycjv321.inventory.{Message, NotFoundError}
import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}

class MovieService @Inject() (movies: MovieRepository)(
    implicit
    val executionContext: ExecutionContext
) {

  def findById(id: Int): MovieFuture = movies.findById(id)

  // TODO move this logic down to repository
  def list: MoviesFuture = movies.list().map(Movies.apply)

  def +=(movie: Movie): UpdatedMessageFuture = movies += movie

  //  TODO move this logic to db  by wrapping in a transaction
  def deleteById(id: Int): Future[Either[Message, Int]] =
    for {
      _ <- fromOptionF[Future, Message, Movie](
        findById(id),
        new NotFoundError(id)
      )
      count <- EitherT[Future, Message, Int](movies.deleteById(id).map {
        case Left(_)      => new DeleteMovieError(id).asLeft
        case Right(count) => count.asRight
      })
    } yield {
      count
    }
}
