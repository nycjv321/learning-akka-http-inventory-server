package com.nycjv321.inventory

import java.time.LocalDate

import scala.concurrent.Future

package object movies {

  case class InventoryError(message: String) extends RuntimeException {
    override def toString: String = message
  }

  type UpdatedMessage       = Either[Message, Movie]
  type UpdatedMessageFuture = Future[UpdatedMessage]
  type MoviesFuture         = Future[Movies]
  type MovieOption          = Option[Movie]
  type MovieFuture          = Future[MovieOption]

  final case class Movie(
      override val id: Id = None,
      title: String,
      releaseDate: LocalDate,
      description: String = ""
  ) extends Identifiable

  final case class Movies(movies: Seq[Movie]) {
    def isEmpty: Boolean = this.movies.isEmpty
  }

  sealed trait MovieError {
    def message: String
  }

  case class DeletedMovie private (override val message: String)
      extends Message(message) {
    def this(id: Int) {
      this(message = s"deleted movie ($id")
    }
  }

  case class DeleteMovieError private (override val message: String)
      extends Message(message)
      with MovieError {
    def this(id: Int) {
      this(message = s"unable to delete movie ${id}")
    }
  }

}
