package com.nycjv321.inventory

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.nycjv321.database.{DatabaseConfiguration, Ping, Migratable}
import com.nycjv321.inventory.movies.{MovieRegistry, MovieService, MoviesRoute}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

object MovieServer extends App with Migratable with InventoryServer {

  implicit val system: ActorSystem             = ActorSystem("inventoryHttpServer")
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  val injector                                 = Modules.asInjectable

  implicit val executionContext: ExecutionContext =
    injector.getInstance(classOf[ExecutionContext])
  private implicit val configuration: DatabaseConfiguration =
    injector.getInstance(classOf[DatabaseConfiguration])
  private val movies = injector.getInstance(classOf[MovieService])
  private val ping   = injector.getInstance(classOf[Ping])

  val moviesRegistryActor: ActorRef = system.actorOf(
    Props(new MovieRegistry(system, movies)),
    name = "movie-registry-actor"
  )
  lazy val routes: Route = new MoviesRoute(system, moviesRegistryActor).routes
  val serverBinding: Future[Http.ServerBinding] =
    Http().bindAndHandle(routes, "localhost", 8080)

  migrateOnSuccess(ping)
  bind(serverBinding)

  Await.result(system.whenTerminated, Duration.Inf)

}
