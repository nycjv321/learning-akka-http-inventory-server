package com.nycjv321.inventory

import akka.actor.ActorSystem
import com.google.inject.{AbstractModule, Guice, Injector}
import com.nycjv321.database.{
  DatabaseConfiguration,
  DatabaseManager,
  Ping,
  DatabaseQueries
}
import com.nycjv321.inventory.movies.MovieService
import com.typesafe.scalalogging.LazyLogging
import net.codingwell.scalaguice.ScalaModule

import scala.concurrent.ExecutionContext

class Modules(implicit system: ActorSystem)
    extends AbstractModule
    with ScalaModule
    with LazyLogging {

  override def configure(): Unit = {

    bind[ExecutionContext].toInstance(
      system.dispatchers.lookup("postgres-dispatcher")
    )
    bind[DatabaseConfiguration].toInstance(databaseConfiguration)
    bind[HttpConfiguration].toInstance(httpConfiguration)
    bind[DatabaseManager]
    bind[DatabaseQueries]
    bind[MovieService]
    bind[Ping]
    bind[ActorSystem].toInstance(system)
  }

  def databaseConfiguration: DatabaseConfiguration =
    DatabaseConfiguration.read.fold((_ => {
      logger.error(s"unable to read database config")
      system.terminate()
      DatabaseConfiguration.error
    }), (config => config))

  def httpConfiguration: HttpConfiguration =
    HttpConfiguration.read.fold((_ => {
      logger.error(s"unable to read http config")
      system.terminate()
      HttpConfiguration.error
    }), (config => config))

}

object Modules {
  def apply(implicit system: ActorSystem): Modules = new Modules

  def asInjectable(implicit system: ActorSystem): Injector = {
    Guice.createInjector(Modules.apply)
  }
}
