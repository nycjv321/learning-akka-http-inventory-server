package com.nycjv321.inventory.movies

import akka.actor.{Actor, ActorLogging, ActorSystem}
import akka.pattern.pipe
import cats.implicits._
import scala.concurrent.ExecutionContext
import MovieRegistry._

object MovieRegistry {
  final case class AddMovie(movie: Movie)
  final case class GetMovie(id: Int)
  final case class DeleteMovie(id: Int)
  final case object GetMovies
}

class MovieRegistry(system: ActorSystem, movies: MovieService)(
    implicit
    val executionContext: ExecutionContext
) extends Actor
    with ActorLogging {

  def receive: Receive = {
    case GetMovies =>
      pipe(movies.list).to(sender())
    case AddMovie(movie) =>
      pipe(movies += movie).to(sender())
    case GetMovie(id) =>
      pipe(movies.findById(id)).to(sender())
    case DeleteMovie(id) =>
      pipe(movies.deleteById(id).map {
        case Left(e)  => e.asLeft
        case Right(m) => m.asRight
      }).to(sender())
  }
}
