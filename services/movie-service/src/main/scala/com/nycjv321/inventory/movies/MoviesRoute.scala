package com.nycjv321.inventory.movies

import akka.actor.{ActorRef, ActorSystem}
import akka.event.Logging
import akka.http.scaladsl.marshalling.ToResponseMarshallable._
import akka.http.scaladsl.server.Directives.{Segment, pathEnd, pathPrefix}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.RouteConcatenation._
import akka.http.scaladsl.server.directives.PathDirectives.path
import akka.pattern.ask
import akka.util.Timeout
import com.nycjv321.inventory.Message
import com.nycjv321.inventory.SimplifiedRoutes
import com.nycjv321.inventory.movies.JsonSupport._
import com.nycjv321.inventory.movies.MovieRegistry._

import scala.concurrent.duration._
import scala.language.postfixOps

class MoviesRoute(system: ActorSystem, actorRef: ActorRef)
    extends SimplifiedRoutes {

  implicit val timeout: Timeout = Timeout(5000 milliseconds)

  lazy val log = Logging(system, classOf[MoviesRoute])

  lazy val routes: Route = pathPrefix("movies") {
    pathEnd {
      getAll((actorRef ? GetMovies).mapTo[Movies]) ~
        post(
          (movie: Movie) => ((actorRef ? AddMovie(movie)).mapTo[UpdatedMessage])
        )
    } ~
      path(Segment) { id =>
        get(id.toInt, (actorRef ? GetMovie(id.toInt)).mapTo[MovieOption]) ~
          delete(
            id.toInt,
            (actorRef ? DeleteMovie(id.toInt)).mapTo[Either[Message, Int]]
          )
      }
  }

}
