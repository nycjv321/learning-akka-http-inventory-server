package com.nycjv321.inventory.movies

import java.time.LocalDate

import com.nycjv321.database.Querys.{and, column, where}
import com.nycjv321.database.{
  FutureRepository,
  FutureDatabaseQueries,
  DatabaseQueries
}
import com.nycjv321.inventory.movies.MovieRepository.insertQuery
import com.nycjv321.inventory.{AddError, ConflictError, Message}
import cats._, cats.data._, cats.implicits._
import doobie._, doobie.implicits._
import doobie.implicits.javatime._
import doobie.util.fragment
import doobie.util.fragment.Fragment
import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}

class MovieRepository(stoner: FutureDatabaseQueries[Movie])(
    implicit
    override val executionContext: ExecutionContext
) extends FutureRepository[Movie](stoner) {
  val AddMovieConflict = Future(
    Left(new ConflictError[Movie].asInstanceOf[Message])
  )

  def exists(title: String, releaseDate: LocalDate): Future[Boolean] =
    super.exists(
      where(column("title") === (title)),
      and(column("release_date") === (releaseDate))
    )

  @Inject()
  def this(
      stoner: DatabaseQueries
  )(implicit executionContext: ExecutionContext) {
    this(new FutureDatabaseQueries[Movie](stoner))
  }

  override def alias: fragment.Fragment = fr"movies"

  def +=(movie: Movie): Future[Either[Message, Movie]] = {
    this.exists(movie.title, movie.releaseDate) flatMap {
      case true => AddMovieConflict
      case false =>
        stoner.insertAndReturn(
          insertQuery(movie.title, movie.releaseDate, movie.description)
        ) map {
          case Left(e)      => AddError(e.toString).asInstanceOf[Message].asLeft
          case Right(movie) => movie.asRight
        }
    }
  }

  private def addError(movie: Movie) = Future(Left(new AddError(movie)))

  override def tableName: String = "movies"
}

object MovieRepository {
  def insertQuery(
      title: String,
      releaseDate: LocalDate,
      description: String
  ): Fragment =
    sql"insert into movies (title, release_date, description) values ($title, $releaseDate, $description)"
}
