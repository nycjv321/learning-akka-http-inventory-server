package com.nycjv321.inventory.games

import cats.implicits._
import cats.data.EitherT
import cats.data.EitherT.fromOptionF
import com.nycjv321.inventory.{Id, Message, NotFoundError}
import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}

class GameService @Inject() (games: GameRepository)(
    implicit
    val executionContext: ExecutionContext
) {
  def +=(game: Game): Future[Either[Message, Game]] = {
    val updatedGame = game.copy(
      system = GameSystemService.find(game.system.name),
      genre = GenreService.find(game.genre.name)
    )
    games += updatedGame
  }

  def update(game: Game): Future[Either[Message, Game]] = {
    val updatedGame = game.copy(
      system = GameSystemService.find(game.system.name),
      genre = GenreService.find(game.genre.name)
    )

    usingId(updatedGame) { id =>
      findById(id).flatMap {
        case Some(_) => games.update(updatedGame)
        case None    => Future(Left(Message.apply("Not found!")))
      }
    }
  }

  def usingId(
      game: Game
  )(f: Int => Future[Either[Message, Game]]): Future[Either[Message, Game]] = {
    game.id match {
      case Some(id) => f(id)
      case None     => Future(Left(Message.apply("Not found!")))
    }
  }

  def deleteById(id: Int): Future[Either[Message, Int]] = {
    (for {
      _ <- fromOptionF[Future, Message, Game](
        findById(id),
        new NotFoundError(id)
      )
      count <- EitherT[Future, Message, Int](games.deleteById(id).map {
        case Left(_)      => new Message(s"$id deleted").asLeft
        case Right(count) => count.asRight
      })
    } yield {
      count
    }).value
  }

  // TODO move this logic down to repository
  def list: Future[Games] = games.list().map(Games.apply)

  def findById(id: Int): Future[Option[Game]] = games.findById(id)

}
