package com.nycjv321.inventory.games

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.model.{StatusCode, StatusCodes}
import akka.http.scaladsl.server.Directives.{Segment, pathEnd, pathPrefix}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.RouteConcatenation._
import akka.http.scaladsl.server.directives.PathDirectives.path
import akka.pattern.ask
import akka.util.Timeout
import com.nycjv321.inventory.Message
import com.nycjv321.inventory.SimplifiedRoutes
import com.nycjv321.inventory.games.GameCatalog.{
  AddGame,
  DeleteGame,
  GetGame,
  GetGames,
  UpdateGame
}
import com.nycjv321.inventory.games.JsonSupport._

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.language.postfixOps

class GameCatalogRoute(system: ActorSystem, actorRef: ActorRef)(
    implicit
    executionContext: ExecutionContext
) extends SimplifiedRoutes {

  implicit val timeout: Timeout = Timeout(5000 milliseconds)

  lazy val routes: Route = pathPrefix("games") {
    pathEnd {
      getAll((actorRef ? GetGames).mapTo[Games]) ~
        post(
          (game: Game) =>
            ((actorRef ? AddGame(game)).mapTo[Either[Message, Game]])
        ) ~ options()
    } ~
      path(Segment) { id =>
        post(
          (game: Game) =>
            (actorRef ? UpdateGame(game)).mapTo[Either[Message, Game]],
          StatusCodes.OK
        ) ~
          delete(
            id.toInt,
            (actorRef ? DeleteGame(id.toInt)).mapTo[Either[Message, Int]]
          ) ~
          get(id.toInt, (actorRef ? GetGame(id.toInt)).mapTo[Option[Game]]) ~
          options()
      }
  } ~ pathPrefix("systems") {
    pathEnd {
      getAll(Future(GameSystemService.list))
    }
  } ~ pathPrefix("genre") {
    pathEnd {
      getAll(Future(GenreService.list))
    }
  }

}
