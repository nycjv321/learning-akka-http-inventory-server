package com.nycjv321.inventory.games

import java.time.LocalDate

import com.nycjv321.database.{
  FutureRepository,
  FutureDatabaseQueries,
  DatabaseQueries
}
import com.nycjv321.inventory.games.GameRepository._
import com.nycjv321.inventory.games.JsonSupport.GameMetadataFormat
import com.nycjv321.inventory.{AddError, ConflictError, Id, Message}
import doobie.util.fragment.Fragment
import javax.inject.Inject
import doobie.util.meta.Meta
import doobie.util.{Read, Write, fragment}
import org.postgresql.util.PGobject
import cats._, cats.data._, cats.implicits._
import doobie._, doobie.implicits._
import doobie.implicits.javatime._
import doobie.postgres.implicits._

import scala.concurrent.{ExecutionContext, Future}

class GameRepository(stoner: FutureDatabaseQueries[Game])(
    implicit
    override val executionContext: ExecutionContext
) extends FutureRepository[Game](stoner) {
  def update(game: Game): Future[Either[Message, Game]] =
    stoner.update(updateQuery(game)) map {
      case Left(e)  => AddError(e.toString).asInstanceOf[Message].asLeft
      case Right(_) => game.asRight
    }

  @Inject()
  def this(
      stoner: DatabaseQueries
  )(implicit executionContext: ExecutionContext) {
    this(new FutureDatabaseQueries[Game](stoner))
  }

  def +=(game: Game): Future[Either[Message, Game]] = {
    stoner.insertAndReturnID(
      insertQuery(
        game.title,
        game.releaseDate,
        game.description,
        game.system,
        game.genre,
        game.metadata
      )
    ) map {
      case Left(e) => AddError(e.toString).asInstanceOf[Message].asLeft
      case Right(id) => {
        game.copy(id = id).asRight
      }
    }
  }

  override implicit def alias: fragment.Fragment = fr"games"

  override def tableName: String = "games"

  val AddMovieConflict = Future(
    Left(new ConflictError[Game].asInstanceOf[Message])
  )

  override def selectFrom(implicit alias: Fragment): Fragment =
    sql"select games.id, title, games.release_date, games.description, system_id, system.name as system_name, genre_id, genre.name as genre_name, metadata from " ++ alias ++
      sql" left join system on games.system_id = system.id" ++
      sql" left join genre on games.genre_id = genre.id"

}

object GameRepository {
  import spray.json._

  def insertQuery(
      title: String,
      releaseDate: LocalDate,
      description: String,
      system: GameSystem,
      genre: Genre,
      gameMetadata: GameMetadata
  ): Fragment =
    sql"insert into games (title, release_date, description, system_id, genre_id, metadata) values ($title, $releaseDate, $description, ${system.id}, ${genre.id}, $gameMetadata)"

  def updateQuery(game: Game): Fragment =
    sql"update games set title = ${game.title}, release_date = ${game.releaseDate}, description = ${game.description}, system_id = ${game.system.id}, genre_id =  ${game.genre.id}, metadata = ${game.metadata} where id = ${game.id}"

  implicit val jsonMeta: Meta[GameMetadata] =
    Meta.Advanced
      .other[PGobject]("json")
      .timap[GameMetadata](a => {
        GameMetadataFormat.read(a.getValue.parseJson)
      })(a => {
        val o = new PGobject
        o.setType("jsonb")
        o.setValue(a.toJson.compactPrint)
        o
      })

  implicit val gameReader: Read[Game] = {
    Read[
      (
          Id,
          String,
          LocalDate,
          String,
          Id,
          String,
          Id,
          String,
          Option[GameMetadata]
      )
    ].map {
      case (
          id,
          title,
          releaseDate,
          description,
          systemId,
          system,
          genreId,
          genre,
          metadata
          ) =>
        Game(
          id = id,
          title = title,
          releaseDate = releaseDate,
          description = description,
          system = GameSystem(id = systemId, name = system),
          genre = Genre(id = genreId, name = genre),
          metadata = metadata.getOrElse(NoGameData)
        )
    }
  }

  implicit val gameWriter: Write[Game] =
    Write[(Id, String, LocalDate, String, Id, Id, GameMetadata)].contramap {
      game =>
        (
          game.id,
          game.title,
          game.releaseDate,
          game.description,
          game.system.id,
          game.genre.id,
          game.metadata
        )
    }

}
