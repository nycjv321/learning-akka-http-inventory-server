package com.nycjv321.inventory.games

object GenreService {
  val UnknownId       = -1
  val Unknown: Genre  = Genre(Some(UnknownId), "Unknown")
  val Platform: Genre = Genre(Some(0), "Platform")

  def list: Seq[Genre] = Seq(Platform)

  def find(name: String): Genre = list.find(_.name == name).getOrElse(Unknown)

}
