package com.nycjv321.inventory.games

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.google.inject.Injector
import com.nycjv321.inventory.template.{
  MigratableInventoryServer => InventoryServer
}

import scala.concurrent.ExecutionContext

object GameCatalogServer extends App with InventoryServer {

  override def injector()(implicit system: ActorSystem) = Modules.asInjectable

  override def route(
      injector: Injector,
      materializer: ActorMaterializer
  )(implicit system: ActorSystem, executionContext: ExecutionContext): Route = {
    val games = injector.getInstance(classOf[GameService])

    val gameActor: ActorRef =
      system.actorOf(
        Props(new GameCatalog(system, games)),
        name = "game-catalog-actor"
      )
    new GameCatalogRoute(system, gameActor).routes
  }

  start(8081)
}
