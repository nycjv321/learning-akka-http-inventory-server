package com.nycjv321.inventory.games

import java.net.URL
import java.time.LocalDate
import java.time.format.DateTimeFormatter

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.nycjv321.inventory.{Message, SnakifiedSprayJsonSupport}
import spray.json.{
  JsArray,
  JsField,
  JsNull,
  JsNumber,
  JsObject,
  JsString,
  JsValue,
  JsonFormat,
  RootJsonFormat,
  deserializationError
}

import scala.util.Try

object JsonSupport extends SprayJsonSupport with SnakifiedSprayJsonSupport {

  implicit val localDateFormat = new JsonFormat[LocalDate] {
    override def write(obj: LocalDate): JsValue =
      JsString(formatter.format(obj))

    override def read(json: JsValue): LocalDate = {
      json match {
        case JsString(lDString) =>
          Try(LocalDate.parse(lDString, formatter))
            .getOrElse(deserializationError(deserializationErrorMessage))
        case _ => deserializationError(deserializationErrorMessage)
      }
    }

    private val formatter = DateTimeFormatter.ISO_LOCAL_DATE
    private val deserializationErrorMessage =
      s"Expected date time in ISO offset date time format ex. ${LocalDate.now().format(formatter)}"
  }

  implicit val genreJsonFormat: RootJsonFormat[Genre] = jsonFormat2(Genre)
  implicit val systemJsonFormat: RootJsonFormat[GameSystem] = jsonFormat2(
    GameSystem
  )

  implicit object GameMetadataFormat extends RootJsonFormat[GameMetadata] {
    def write(c: GameMetadata): JsObject = {
      c match {
        case EmptyMetadata() => JsObject(("metadata", JsNull))
        case GameMetadataV0(trailers @ _*) =>
          JsObject(
            (
              "metadata",
              JsArray.apply(
                trailers
                  .map(
                    trailer =>
                      JsObject(
                        ("description", JsString(trailer.description)),
                        ("url", JsString(trailer.url.toString))
                      )
                  )
                  .toVector
              )
            )
          )
      }

    }

    override def read(json: JsValue): GameMetadata = {
      val fields = json.asJsObject.fields
      fields.get("version") match {
        case Some(version) if version.convertTo[Int] == 0 =>
          fields.get("trailers") match {
            case Some(trailers: JsArray) =>
              val urls = trailers.elements.map { trailer =>
                val trailerFields = trailer.asJsObject.fields
                val description = trailerFields
                  .get("description")
                  .map(_.convertTo[String])
                  .getOrElse("")
                val url = new URL(
                  trailerFields.get("url").map(_.convertTo[String]) match {
                    case Some(value) => value
                    case None        => "" // this will error
                  }
                )
                UrlWithDescription(description = description, url = url)
              }

              GameMetadataV0(urls: _*)
            case None => NoGameData
          }
        case _ => NoGameData
      }
    }
  }

  implicit object GamesFormat extends RootJsonFormat[Games] {
    override def read(json: JsValue): Games = {
      Games(Seq.empty)
    }

    override def write(games: Games): JsValue =
      JsObject(
        ("games", JsArray(games.map[JsValue](GameFormat.write).toVector))
      )
  }

  implicit object GameFormat extends RootJsonFormat[Game] {
    override def read(json: JsValue): Game = {
      val fields = json.asJsObject("{Invalid JSON Object}").fields

      if (fields.contains("message")) {
        throw new NoSuchElementException("not a game")
      } else {
        val id = fields
          .get("id")
          .fold(None.asInstanceOf[Option[Int]])(a => a.convertTo[Option[Int]])
        val title = fields.get("title").fold("")(_.convertTo[String])
        val description =
          fields.get("description").fold("")(_.convertTo[String])
        val releaseDate =
          fields.get("release_date").map(_.convertTo[LocalDate]).getOrElse {
            throw new NoSuchElementException("release_date is required")
          }

        val system = GameSystemService.find(
          fields.get("system").fold("")(_.convertTo[String])
        )
        val genre =
          GenreService.find(fields.get("genre").fold("")(_.convertTo[String]))

        val metadata = fields
          .get("metadata")
          .map(metadata => {
            metadata.convertTo[GameMetadata]
          })
          .getOrElse(NoGameData)

        Game(
          id = id,
          title = title,
          description = description,
          releaseDate = releaseDate,
          genre = genre,
          system = system,
          metadata = metadata
        )
      }
    }

    override def write(game: Game): JsValue = {

      def description(games: Game): Option[JsField] = {
        games.description match {
          case ""          => None
          case description => Some("description", JsString(description))
        }
      }

      def metadata(games: Game): Option[List[JsField]] = {
        games.metadata match {
          case EmptyMetadata() => None
          case GameMetadataV0(trailers @ _*) =>
            Some(
              List[JsField](
                (
                  "metadata",
                  JsObject(
                    ("version", JsNumber(game.metadata.version)),
                    (
                      "trailers",
                      JsArray(
                        trailers
                          .map(
                            trailer =>
                              JsObject(
                                (
                                  "description",
                                  JsString.apply(trailer.description)
                                ),
                                ("url", JsString.apply(trailer.url.toString))
                              )
                          )
                          .toVector
                      )
                    )
                  )
                )
              )
            )
        }
      }

      val optionalFields: List[JsField] =
        (metadata(game), description(game)) match {
          case (Some(m), Some(d)) => m :+ d
          case (Some(m), None)    => m
          case (None, Some(d))    => List(d)
          case (None, None)       => List.empty
        }

      val fields: List[JsField] = List(
        ("id", JsNumber.apply(game.id.getOrElse(0))),
        ("title", JsString.apply(game.title)),
        ("release_date", localDateFormat.write(game.releaseDate)),
        ("genre", JsString.apply(game.genre.name)),
        ("system", JsString.apply(game.system.name))
      ) ++ optionalFields

      JsObject(fields: _*)
    }
  }

}
