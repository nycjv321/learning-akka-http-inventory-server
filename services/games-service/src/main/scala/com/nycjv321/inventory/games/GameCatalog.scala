package com.nycjv321.inventory.games

import akka.actor.{Actor, ActorLogging, ActorSystem}

import scala.concurrent.{ExecutionContext, Future}
import akka.pattern.pipe
import com.nycjv321.inventory.games.GameCatalog.{
  AddGame,
  DeleteGame,
  GetGame,
  GetGames,
  UpdateGame
}

object GameCatalog {
  final case class AddGame(game: Game)
  final case class UpdateGame(game: Game)
  final case class GetGame(id: Int)
  final case class DeleteGame(id: Int)
  final case object GetGames

}

class GameCatalog(system: ActorSystem, games: GameService)(
    implicit
    val executionContext: ExecutionContext
) extends Actor
    with ActorLogging {
  override def receive: Receive = {
    case AddGame(game) =>
      pipe(games += game).to(sender())
    case UpdateGame(game) =>
      pipe(games.update(game)).to(sender())
    case GetGame(id) =>
      pipe(games.findById(id)).to(sender())
    case DeleteGame(game) =>
      pipe(games.deleteById(game)).to(sender())
    case GetGames =>
      pipe(games.list).to(sender())
  }
}
