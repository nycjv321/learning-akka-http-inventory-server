package com.nycjv321.inventory.games

object GameSystemService {
  val UnknownId                  = -1
  val Unknown: GameSystem        = GameSystem(Some(UnknownId), "Unknown")
  val NintendoSwitch: GameSystem = GameSystem(Some(0), "Nintendo Switch")

  def list: Seq[GameSystem] = Seq(NintendoSwitch)

  def find(name: String): GameSystem =
    list.find(_.name == name).getOrElse(Unknown)

}
