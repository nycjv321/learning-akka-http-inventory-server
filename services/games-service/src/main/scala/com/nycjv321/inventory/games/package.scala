package com.nycjv321.inventory

import java.net.URL
import java.time.LocalDate

package object games {

  case class GameSystem(id: Id = None, name: String)

  case class Genre(id: Id = None, name: String)

  sealed trait GameMetadata {
    def version: Version
  }

  case class EmptyMetadata() extends GameMetadata {
    override def version: Version = -1
  }

  case class GameMetadataV0(trailers: UrlWithDescription*)
      extends GameMetadata {
    override def version: Version = 0
  }

  val NoGameData = EmptyMetadata()

  final case class Game(
      id: Id = None,
      title: String,
      releaseDate: LocalDate,
      description: String,
      system: GameSystem,
      genre: Genre,
      metadata: GameMetadata = NoGameData
  ) extends Identifiable

  final case class Games(games: Seq[Game]) {
    def isEmpty: Boolean             = this.games.isEmpty
    def map[B](f: Game => B): Seq[B] = games.map(f)
  }

  case class UrlWithDescription(description: String, url: URL)

}
