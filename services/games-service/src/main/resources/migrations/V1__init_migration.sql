create table if not exists system (
    id           serial primary key,
    name         varchar(300) not null unique,
    release_date timestamp,
    description  text,
    created      timestamp with time zone default now() not null,
    modified     timestamp
);

create table if not exists genre (
    id           serial primary key,
    name         varchar(300)  not null unique,
    description  text,
    created      timestamp with time zone default now() not null,
    modified     timestamp

);

create table if not exists games
(
    id           serial primary key,
    title         varchar(300) not null,
    release_date timestamp,
    description  text,
    system_id    int references system(id),
    genre_id     int references genre(id),
    created      timestamp with time zone default now() not null,
    modified     timestamp,
    constraint uniq_game unique (title, system_id)
);

INSERT INTO genre (id, name) VALUES (0, 'Platform');
INSERT INTO system (id, name) VALUES (0, 'Nintendo Switch');

create function update_modified_column() returns trigger
    language plpgsql
as
$$
BEGIN
    NEW.modified = now();
    RETURN NEW;
END;
$$;

CREATE TRIGGER update_system_modified_time
    BEFORE UPDATE
    ON system
    FOR EACH ROW
EXECUTE PROCEDURE update_modified_column();

CREATE TRIGGER update_genre_modified_time
    BEFORE UPDATE
    ON genre
    FOR EACH ROW
EXECUTE PROCEDURE update_modified_column();

CREATE TRIGGER update_games_modified_time
    BEFORE UPDATE
    ON games
    FOR EACH ROW
EXECUTE PROCEDURE update_modified_column();