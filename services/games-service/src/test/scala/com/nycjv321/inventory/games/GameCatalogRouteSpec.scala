package com.nycjv321.inventory.games

import java.net.URL
import java.time.LocalDate

import akka.actor.Props
import com.google.inject.Injector
import com.nycjv321.database.{DatabaseConfiguration, Migratable, Ping}
import com.nycjv321.inventory.test.RouteSpec
import com.nycjv321.inventory.games.{GameMetadataV0 => GameMetadata}

class GameCatalogRouteSpec extends RouteSpec with Migratable {
  override val injector: Injector = Modules.asInjectable

  private implicit val databaseConfiguration =
    injector.getInstance(classOf[DatabaseConfiguration])
  private implicit val gameRepository =
    injector.getInstance(classOf[GameRepository])

  val gameService = injector.getInstance(classOf[GameService])
  lazy val routes = new GameCatalogRoute(
    system,
    system.actorOf(
      Props(new GameCatalog(system, gameService)),
      name = "game-catalog-actor"
    )
  ).routes
  migrateOnSuccess(injector.getInstance(classOf[Ping]))
  var game: Game = _

  "GameCatalog" should {
//     make this synchronous
    for {
      result <- gameRepository.deleteAll
    } yield {
      result match {
        case Left(error) => fail(error)
        case Right(_)    =>
      }
    }
    val gameCatalog = new GameCatalogClient(routes)

    "return no movies if nil" in {
      assert(gameCatalog.getGames().isEmpty)
    }

    "be able to add games" in {
      game = gameCatalog.add(
        Game(
          title = "The Legend of Zelda™: Link’s Awakening Nintendo Switch",
          releaseDate = LocalDate.of(2019, 9, 20),
          description = "Castaway, you should know the truth!",
          genre = Genre(None, "Platform"),
          system = GameSystem(None, "Nintendo Switch")
        )
      )
      assertIdentifiable(game)
    }

    "be able to add games with (version 0) metadata" in {
      val superMarioOdyssey = Game(
        title = "Super Mario Odyssey",
        releaseDate = LocalDate.of(2017, 10, 27),
        description = "",
        genre = Genre(None, "Platform"),
        system = GameSystem(None, "Nintendo Switch"),
        metadata = GameMetadata(
          trailers = UrlWithDescription(
            description =
              "Super Mario Odyssey - Game Trailer - Nintendo E3 2017",
            url = new URL("https://youtu.be/wGQHQc_3ycE")
          ),
          UrlWithDescription(
            description =
              "Super Mario Odyssey - Nintendo Switch Presentation 2017 Trailer",
            url = new URL("https://youtu.be/5kcdRBHM7kM")
          )
        )
      )
      val game = gameCatalog.add(
        superMarioOdyssey
      )
      assertIdentifiable(game)
      val resolvedGame = superMarioOdyssey.copy(
        id = game.id,
        genre = GenreService.find(superMarioOdyssey.genre.name),
        system = GameSystemService.find(superMarioOdyssey.system.name)
      )
      assert(game == resolvedGame)
    }

    "be able to retrieve an individual game" in {
      gameCatalog.get(game.id) match {
        case Left(message) =>
          fail(s"did not expect to receive an error: $message")
        case Right(resolvedMovie) => resolvedMovie should ===(game)
      }
    }

    // TODO make this test more thorough
    "be able to update games game" in {
      val updatedGame = game.copy(title = game.title + " copy")
      gameCatalog.update(updatedGame) match {
        case Left(message) =>
          fail(s"did not expect to receive an error: $message")
        case Right(resolvedMovie) => resolvedMovie should ===(updatedGame)
      }
    }

    // TODO add more tests

  }

}
