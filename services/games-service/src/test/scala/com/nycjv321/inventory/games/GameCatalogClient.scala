package com.nycjv321.inventory.games

import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.{
  ContentType,
  ContentTypes,
  MessageEntity,
  StatusCode,
  StatusCodes
}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.concurrent.ScalaFutures
import JsonSupport._
import com.nycjv321.inventory.{Id, Message}

class GameCatalogClient(routes: Route)(
    implicit val routeTest: ScalatestRouteTest
) extends ScalaFutures {
  import routeTest._

  def getGames(
      statusCode: StatusCode = StatusCodes.OK,
      contentType: ContentType = ContentTypes.`application/json`
  ): Games = {
    val request = routeTest.Get(uri = "/games")
    request ~> routes ~> check {
      verifyHeaders(statusCode, contentType)
      entityAs[Games]
    }
  }

  def add(
      game: Game,
      statusCode: StatusCode = StatusCodes.Created,
      contentType: ContentType = ContentTypes.`application/json`
  ): Game = {
    val gameEntity = Marshal(game).to[MessageEntity].futureValue // futureValue is from ScalaFutures
    val request    = Post("/games").withEntity(gameEntity)
    request ~> routes ~> check {
      verifyHeaders(statusCode, contentType)
      entityAs[Game]
    }
  }

  def get(
      id: Id,
      statusCode: StatusCode = StatusCodes.OK,
      contentType: ContentType = ContentTypes.`application/json`
  ): Either[Message, Game] = {
    val request = routeTest.Get(
      uri = s"/games/${id.getOrElse(failTest("game did not have id"))}"
    )
    request ~> routes ~> check {
      verifyHeaders(statusCode, contentType)
      entityAs[Either[Message, Game]]
    }
  }

  def update(
      game: Game,
      statusCode: StatusCode = StatusCodes.OK,
      contentType: ContentType = ContentTypes.`application/json`
  ): Either[Message, Game] = {
    val gameEntity = Marshal(game).to[MessageEntity].futureValue
    val request = routeTest
      .Post(
        uri = s"/games/${game.id.getOrElse(failTest("game did not have id"))}"
      )
      .withEntity(gameEntity)
    request ~> routes ~> check {
      verifyHeaders(statusCode, contentType)
      entityAs[Either[Message, Game]]
    }
  }

  private def verifyHeaders(
      statusCode: StatusCode = StatusCodes.OK,
      contentType: ContentType = ContentTypes.`application/json`
  )(implicit routeTest: ScalatestRouteTest): Unit = {
    assert(
      routeTest.status == statusCode,
      s"${routeTest.status} == $statusCode, ${entityAs[String]}"
    )
    assert(
      routeTest.contentType == contentType,
      s"${routeTest.status} == $statusCode, ${entityAs[String]}"
    )
  }

}
