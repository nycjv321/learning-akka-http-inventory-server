package com.nycjv321.inventory.file

import java.util.UUID

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.model.HttpEntity.{ChunkStreamPart, Chunked}
import akka.http.scaladsl.model.{ContentTypes, HttpResponse, StatusCodes}
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Directives.{pathEnd, pathPrefix, _}
import akka.http.scaladsl.server.directives.MethodDirectives
import akka.http.scaladsl.server.directives.PathDirectives.path
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.http.scaladsl.server.{Directive0, Route}
import akka.stream.{IOResult, Materializer}
import akka.stream.scaladsl.{FileIO, Sink, Source}
import akka.util.Timeout
import cats.data.EitherT
import cats.implicits._
import com.nycjv321.inventory
import com.nycjv321.inventory.SimplifiedRoutes
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._
import scala.language.postfixOps

class FileRoute(
    system: ActorSystem,
    storedObjectService: StoredObjectService
)(
    implicit
    val materializer: Materializer,
    implicit val executionContext: ExecutionContext
) extends FileJsonSupport
    with LazyLogging
    with SimplifiedRoutes {
  implicit val timeout: Timeout = Timeout(5000 milliseconds)

  lazy val log = Logging(system, classOf[FileRoute])

  lazy val routes: Route = pathPrefix("files") {
    pathEnd {
      MethodDirectives.post {
        fileUpload("csv") {
          case (metadata, byteSource) =>
            storedObjectService
              .add(metadata.fileName, byteSource)
              .okay(addLocationHeader)
        }
      }
    } ~
      path(Segment) { id =>
        MethodDirectives.get {
          // https://stackoverflow.com/questions/49011621/how-to-send-a-file-as-a-response-using-akka-http
          onSuccess(storedObjectService.get(UUID.fromString(id))) {
            case Left(message) =>
              MethodDirectives.get {
                complete((StatusCodes.NotFound, message))
              }
            case Right(io) =>
              complete(fileEntityResponse(io.getAbsolutePath, "UTF8"))
          }
        }
      }

  }
  val fileContentsSource: (String, String) => Source[ChunkStreamPart, _] =
    (fileName, enc) =>
      Source
        .fromIterator(scala.io.Source.fromFile(fileName, enc).getLines)
        .map(a => ChunkStreamPart.apply(a + "\n"))

  val fileEntityResponse: (String, String) => HttpResponse =
    (fileName, enc) =>
      HttpResponse(
        entity = Chunked(
          ContentTypes.`text/plain(UTF-8)`,
          fileContentsSource(fileName, enc)
        )
      )

  //  https://doc.akka.io/docs/akka-http/current/routing-dsl/directives/file-and-resource-directives/getFromFile.html

  def addLocationHeader(file: StoredObject): Directive0 = {
    respondWithHeaders(
      RawHeader("Location", s"/files/${file.id.getOrElse(-1)}")
    )
  }
}
