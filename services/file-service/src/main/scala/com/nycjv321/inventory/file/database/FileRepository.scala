package com.nycjv321.inventory.file.database

import java.io.File
import java.nio.file.Paths
import java.time.LocalDateTime
import java.util.UUID

import com.nycjv321.inventory.file.StoredObject
import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}

class FileRepository @Inject() (val ctx: PostgresContext)(
    implicit val executionContext: ExecutionContext
) extends FileSchema {
  import ctx._

  def +=(storedObject: StoredObject): Future[Option[StoredObject]] = {
    val query = quote {
      files
        .insert(
          _.name -> lift(storedObject.name),
          _.path -> lift(storedObject.path)
        )
        .returning(result => (result.id))
    }
    for {
      result <- ctx.run(query)
    } yield {
      result match {
        case id =>
          Option(
            storedObject.copy(id = id, created = Option(LocalDateTime.now()))
          )
        case _ => None
      }
    }
  }

  def findById(id: UUID): Future[Option[File]] = {
    val query = quote {
      files.filter(_.id == lift(Option(id))).map(o => o.path)
    }

    for {
      result <- ctx.run(query)
    } yield {
      result.headOption.map(path => new File(s"$path/$id"))
    }
  }

  def list: Future[List[StoredObject]] = {
    val filteredList = quote {
      files
    }

    ctx.run(filteredList)
    for {
      result <- ctx.run(filteredList)
    } yield {
      result
    }
  }

  def contains(storedObject: StoredObject): Future[Boolean] = {
    val filteredList = quote {
      files
        .filter(
          a =>
            a.name == lift(storedObject.name) && a.path == lift(
              storedObject.path
            )
        )
        .nonEmpty
    }
    for {
      result <- ctx.run(filteredList)
    } yield {
      result
    }

  }

}
