package com.nycjv321.inventory.file

import com.typesafe.config.ConfigFactory
import io.getquill.{PostgresAsyncContext, SnakeCase}

package object database {

  class PostgresContext
      extends PostgresAsyncContext(
        SnakeCase,
        ConfigFactory.load().getConfig("ctx")
      )

}
