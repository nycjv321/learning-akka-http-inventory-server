package com.nycjv321.inventory.file

import java.io.File
import java.net.URI
import java.nio.file.Paths
import java.util.UUID

import akka.stream.scaladsl.{FileIO, Source}
import akka.stream.{IOResult, Materializer}
import akka.util.ByteString
import cats.data.EitherT
import cats.implicits._
import com.nycjv321.inventory.file.PathHelpers.{
  appendToAbsolutePath,
  createFile
}
import com.nycjv321.inventory.file.database.FileRepository
import com.nycjv321.inventory.{Message, futureEitherTToFutureEither}
import com.typesafe.scalalogging.LazyLogging
import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class StoredObjectService @Inject() (
    storedObjectRepository: FileRepository,
    implicit val pathConfiguration: PathConfiguration
)(
    implicit val executionContext: ExecutionContext
) extends LazyLogging {

  def writeStoredObject(
      storedObject: StoredObject,
      byteSource: Source[ByteString, Any]
  )(
      implicit
      materializer: Materializer
  ): Future[Either[Message, StoredObject]] = {
    val id   = storedObject.id.getOrElse("-1")
    val path = Paths.get(new URI("file://" + storedObject.path))
    for {
      destination <- EitherT.cond[Future](
        createFile(path),
        appendToAbsolutePath(path, s"/$id"),
        Message("unable to create file")
      )
      result <- EitherT.right(byteSource.runWith(FileIO.toPath(destination)))
      processedResults <- EitherT.fromEither[Future](
        processResult(result, storedObject)
      )
    } yield processedResults
  }

  def processResult(
      result: IOResult,
      storedObject: StoredObject
  ): Either[Message, StoredObject] = {
    result.status match {
      case Failure(exception) => Message(exception.getMessage).asLeft
      case Success(_)         => storedObject.asRight
    }
  }

  def get(id: UUID): Future[Either[Message, File]] = {
    for {
      optFile <- this.storedObjectRepository.findById(id)
    } yield {
      optFile match {
        case Some(file) => Right(file)
        case None       => Left(Message(s"file (${id} not found"))
      }
    }
  }

  def add(fileName: String, byteSource: Source[ByteString, Any])(
      implicit
      materializer: Materializer
  ): Future[Either[Message, StoredObject]] = {
    this.+=(fileName, byteSource)
  }

  def +=(fileName: String, byteSource: Source[ByteString, Any])(
      implicit
      materializer: Materializer
  ): Future[Either[Message, StoredObject]] = {
    val storedObject = StoredObject.apply(
      name = fileName,
      configuration = StoredObjectConfiguration()
    )
    for {
      updatedObject <- EitherT.fromOptionF(
        this.storedObjectRepository += storedObject,
        Message("could not update")
      )
      uploadResult <- EitherT(writeStoredObject(updatedObject, byteSource))
    } yield {
      uploadResult
    }
  }

}
