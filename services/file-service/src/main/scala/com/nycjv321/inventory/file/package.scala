package com.nycjv321.inventory

import java.time.LocalDateTime

import io.getquill.Embedded

package object file {
  case class StoredObject(
      id: UUIDIdentifier,
      name: String,
      path: String,
      configuration: StoredObjectConfiguration = StoredObjectConfiguration(),
      created: Option[LocalDateTime] = None,
      modified: Option[LocalDateTime] = None
  ) extends IdentifiableUUID

  object StoredObject {
    def apply(name: String, configuration: StoredObjectConfiguration)(
        implicit pathConfiguration: PathConfiguration
    ): StoredObject = {
      val created = LocalDateTime.now()
      val path    = PathHelpers.toPath(created).toAbsolutePath.toString
      new StoredObject(None, name, path, configuration, Option(created), None)
    }
  }

  case class PathConfiguration(prefix: Option[String])

  sealed trait ObjectConfiguration
  case class StoredObjectConfiguration()
      extends ObjectConfiguration
      with Embedded

}
