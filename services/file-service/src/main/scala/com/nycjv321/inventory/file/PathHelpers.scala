package com.nycjv321.inventory.file

import java.io.{File, IOException}
import java.nio.file.{Files, Path, Paths}
import java.time.LocalDateTime

object PathHelpers {

  private def extractPathComponents(
      dateTime: LocalDateTime
  )(implicit pathConfiguration: PathConfiguration): Seq[String] = {
    val year  = dateTime.getYear
    val month = dateTime.getMonthValue
    val day   = dateTime.getDayOfMonth
    val hour  = dateTime.getHour
    (Seq(year, month, day, hour).map(_.toString), pathConfiguration.prefix) match {
      case (x :: xs, None)         => x :: xs
      case (x :: xs, Some(prefix)) => s"$prefix/$x" :: xs
    }
  }

  def toPath(
      dateTime: LocalDateTime
  )(implicit pathConfiguration: PathConfiguration): Path = {
    extractPathComponents(dateTime) match {
      case x :: xs => Paths.get(x, xs: _*)
    }
  }

  def appendToAbsolutePath(path: Path, suffix: String): Path = {
    Paths.get(path.toAbsolutePath + s"/$suffix")

  }

  def createFile(path: Path, createParents: Boolean = true): Boolean = {
    if (!Files.exists(path)) {
      try {
        if (createParents) {
          Files.createDirectories(path.getParent)
        }
        new File(path.toString).mkdir()
      } catch {
        case _: IOException => false
      }
    } else {
      true
    }
  }

}
