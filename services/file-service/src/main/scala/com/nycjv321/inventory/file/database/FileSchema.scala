package com.nycjv321.inventory.file.database

import com.nycjv321.inventory.file.StoredObject
import io.getquill._

trait FileSchema {
  val ctx: PostgresContext
  import ctx._

  implicit val meta: InsertMeta[StoredObject] =
    insertMeta[StoredObject](_.id, _.created, _.modified)

  val files = quote {
    querySchema[StoredObject]("files")
  }
}
