package com.nycjv321.inventory.file

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.google.inject.Injector
import com.nycjv321.inventory.template.{
  MigratableInventoryServer => InventoryServer
}

import scala.concurrent.ExecutionContext

object FileServer extends App with InventoryServer {

  override def injector()(implicit system: ActorSystem) = Modules.asInjectable

  override def route(
      injector: Injector,
      materializer: ActorMaterializer
  )(implicit system: ActorSystem, executionContext: ExecutionContext): Route = {
    val storedObjectService = injector.getInstance(classOf[StoredObjectService])
    new FileRoute(system, storedObjectService)(
      materializer,
      executionContext
    ).routes
  }

  start()
}
