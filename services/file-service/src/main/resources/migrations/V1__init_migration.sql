create table if not exists files (
    id           uuid primary key default uuid_generate_v4(),
    name         varchar(500),
    path         varchar(500),
    configuration  jsonb not null default '{}',
    created      timestamp default now() not null,
    modified     timestamp
);

create unique index unique_file on files (name, path);

create function update_modified_column() returns trigger
    language plpgsql
as
$$
BEGIN
    NEW.modified = now();
    RETURN NEW;
END;
$$;

CREATE TRIGGER update_files_modified_time BEFORE UPDATE ON files FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();
