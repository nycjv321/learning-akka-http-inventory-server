package com.nycjv321.inventory.file

import java.util.UUID

import com.google.inject.Injector
import com.nycjv321.database.{DatabaseConfiguration, Migratable, Ping}
import com.nycjv321.inventory.file.clients.FileClient
import com.nycjv321.inventory.test.RouteSpec

import scala.util.Random

class FileRouteSpec extends RouteSpec with Migratable {
  override val injector: Injector = Modules.asInjectable

  private implicit val databaseConfiguration =
    injector.getInstance(classOf[DatabaseConfiguration])
  private val storedObjectService: StoredObjectService =
    injector.getInstance(classOf[StoredObjectService])
  lazy val routes = new FileRoute(
    system,
    storedObjectService
  ).routes

  migrateOnSuccess(injector.getInstance(classOf[Ping]))

  val files = FileClient(routes)

  def createFile(): (UUID, String) = {
    val payload = "2,3,5\n7,11,13,17,23\n29,31,37\n"
    (
      files
        .upload(
          payload,
          Random.alphanumeric.take(10).toList.mkString(""),
          "csv"
        )
        .getOrElse(fail),
      payload
    )
  }
  "file upload works" in {
    val (identifier, payload) = createFile()
    assert(identifier != null)
    assert(payload.nonEmpty)
  }

  "file download works" in {
    val (identifier, payload) = createFile()
    val response              = files.download(identifier)
    assert(response == payload)
  }

}
