package com.nycjv321.inventory.file.clients

import java.util.UUID

import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{
  ContentTypes,
  HttpEntity,
  Multipart,
  StatusCodes
}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.nycjv321.inventory.UUIDIdentifier
import org.scalatest.concurrent.ScalaFutures

case class FileClient(routes: Route)(implicit val routeTest: ScalatestRouteTest)
    extends ScalaFutures {
  import routeTest._

  def upload(
      payload: String,
      fileName: String,
      extension: String
  ): UUIDIdentifier = {
    val multipartForm =
      Multipart.FormData(
        Multipart.FormData.BodyPart.Strict(
          extension,
          HttpEntity(ContentTypes.`text/plain(UTF-8)`, payload),
          Map("filename" -> fileName)
        )
      )

    routeTest.Post(uri = "/files", multipartForm) ~> routes ~> check {
      val response     = responseAs[String]
      val location     = header("Location")
      val actualHeader = location.map(_.value()).getOrElse("")
      assert(
        actualHeader.startsWith("/files/"),
        s"${actualHeader} doesn't start with /files"
      )

      assert(
        status == StatusCodes.OK,
        s"Expected $status == ${StatusCodes.OK}, see $response"
      )
      Option(UUID.fromString(actualHeader.replace("/files/", "")))
    }
  }

  def download(id: UUID): String = {

    /**
      * Get("/logs/example") ~> route ~> check {
      * responseAs[String] shouldEqual "example file contents"
      * }
      */
    routeTest.Get(uri = s"/files/$id") ~> routes ~> check {
      assert(
        status == StatusCodes.OK
      )

      val response = responseAs[String]
      response
    }
  }

}
