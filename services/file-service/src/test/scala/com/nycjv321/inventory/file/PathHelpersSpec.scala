package com.nycjv321.inventory.file
import java.time.LocalDateTime

import com.nycjv321.inventory.file.PathHelpers.toPath
import org.scalatest._
import org.scalatest.wordspec.AnyWordSpec

class PathHelpersSpec extends AnyWordSpec {
  implicit val pathConfiguration: PathConfiguration = PathConfiguration(
    Option("/root")
  )
  val expectedDate: LocalDateTime = LocalDateTime.of(1999, 10, 20, 5, 1, 3)

  "PathHelpers" can {
    "generate paths" should {
      "create paths from a local date" in {
        val path         = toPath(expectedDate)
        val actualPath   = path.toAbsolutePath.toString
        val expectedPath = "/root/1999/10/20/5"
        assert(actualPath === expectedPath)
      }
    }
  }

}
