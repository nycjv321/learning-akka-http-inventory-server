package com.nycjv321.inventory.template

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.google.inject.Injector
import com.nycjv321.database.{DatabaseConfiguration, Migratable, Ping}
import com.nycjv321.inventory.{
  HttpConfiguration,
  InventoryServer,
  ServerPortNumber,
  ImplicitServerPortNumber
}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

trait MigratableInventoryServer extends Migratable with InventoryServer {
  def route(injector: Injector, materializer: ActorMaterializer)(
      implicit
      system: ActorSystem,
      executionContext: ExecutionContext
  ): Route

  def injector()(implicit system: ActorSystem): Injector

  def start(port: ServerPortNumber = -1): Unit = {
    implicit val system: ActorSystem = ActorSystem("inventoryHttpServer")
    val dependencies                 = injector()
    implicit val executionContext: ExecutionContext =
      dependencies.getInstance(classOf[ExecutionContext])
    implicit val databaseConfig: DatabaseConfiguration =
      dependencies.getInstance(classOf[DatabaseConfiguration])
    val httpConfig: HttpConfiguration =
      dependencies.getInstance(classOf[HttpConfiguration])
    val ping: Ping                               = dependencies.getInstance(classOf[Ping])
    implicit val materializer: ActorMaterializer = ActorMaterializer()

    val serverConfig = httpConfig.server
    val serverBinding: Future[Http.ServerBinding] = Http().bindAndHandle(
      route(dependencies, materializer),
      serverConfig.host,
      port.getOrElse(serverConfig.port)
    )
    bind(serverBinding)
    migrateOnSuccess(ping)
    Await.result(system.whenTerminated, Duration.Inf)

  }
}
