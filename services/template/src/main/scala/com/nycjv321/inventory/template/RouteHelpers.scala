package com.nycjv321.inventory.template

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives.onSuccess
import akka.http.scaladsl.server.{Directive0, Route}
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import com.nycjv321.inventory.{Message, SnakifiedSprayJsonSupport}

import scala.concurrent.Future

trait RouteHelpers extends SprayJsonSupport with SnakifiedSprayJsonSupport {

  implicit class ResponsibleFuture[A](response: Future[Either[Message, A]]) {

    def okay(
        handleRoute: A => Directive0,
        onError: StatusCodes.ServerError = StatusCodes.InternalServerError
    ): Route = {
      onSuccess(response) {
        case Left(value) => {
          complete((onError, value))
        }
        case Right(entity) =>
          handleRoute(entity) {
            complete((StatusCodes.OK, None))
          }
      }
    }

  }
}
