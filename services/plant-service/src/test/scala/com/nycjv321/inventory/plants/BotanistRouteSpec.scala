package com.nycjv321.inventory.plants

import akka.actor.Props
import akka.http.scaladsl.server.Route
import com.google.inject.Injector
import com.nycjv321.database.{DatabaseConfiguration, Migratable, Ping}
import com.nycjv321.inventory.plants.client.PlantClient
import com.nycjv321.inventory.test.RouteSpec

class BotanistRouteSpec extends RouteSpec with Migratable {
  override val injector: Injector = Modules.asInjectable
  private val plantService = injector.getInstance(classOf[PlantService])
  private implicit val databaseConfiguration = injector.getInstance(classOf[DatabaseConfiguration])

  lazy val routes = new BotanistRoute(system, system.actorOf(Props(new Botanist(system, plantService)), name = "botanist-actor-routes")).routes

  migrateOnSuccess(injector.getInstance(classOf[Ping]))

  val plants = PlantClient(routes)
  "return no plants if nil" in {
    // note that there's no need for the host part in the uri:
    assert(plants.getPlants().isEmpty)
  }


}

