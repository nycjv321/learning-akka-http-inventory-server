package com.nycjv321.inventory.plants.client

import akka.http.scaladsl.model.{ContentType, ContentTypes, StatusCode, StatusCodes}
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.concurrent.ScalaFutures
import com.nycjv321.inventory.plants.PlantJsonSupport._
import com.nycjv321.inventory.plants.Plants

case class PlantClient(routes: Route)(implicit val routeTest: ScalatestRouteTest) extends ScalaFutures {

  import routeTest._

  def getPlants(
                 statusCode: StatusCode = StatusCodes.OK,
                 contentType: ContentType = ContentTypes.`application/json`): Plants = {
    val request = routeTest.Get(uri = "/plants")
    request ~> routes ~> check {
      verifyHeaders(statusCode, contentType)
      entityAs[Plants]
    }
  }

  private def verifyHeaders(
                             statusCode: StatusCode = StatusCodes.OK,
                             contentType: ContentType = ContentTypes.`application/json`)(implicit routeTest: ScalatestRouteTest): Unit = {
    assert(routeTest.status == statusCode, s"${routeTest.status} == $statusCode, ${entityAs[String]}")
    assert(routeTest.contentType == contentType, s"${routeTest.status} == $statusCode, ${entityAs[String]}")
  }

}