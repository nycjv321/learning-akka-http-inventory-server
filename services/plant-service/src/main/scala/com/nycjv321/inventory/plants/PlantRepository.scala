package com.nycjv321.inventory.plants

import java.sql.Timestamp

import cats.data.EitherT
import cats.implicits._
import com.nycjv321.database.{FutureRepository, FutureDatabaseQueries, DatabaseQueries}
import com.nycjv321.inventory.plants.PlantJsonSupport._
import com.nycjv321.inventory.plants.PlantRepository._
import com.nycjv321.inventory.{AddError, ConflictError, Id, Message, Version}
import doobie.implicits._
import doobie.util.fragment.Fragment
import cats._
import cats.data._
import cats.implicits._
import doobie._
import doobie.implicits._
import doobie.implicits.javatime._
import doobie.util.fragment
import doobie.postgres.implicits._

import javax.inject.Inject
import org.postgresql.util.PGobject

import scala.concurrent.{ExecutionContext, Future}

class PlantRepository(stoner: FutureDatabaseQueries[Plant])(implicit override val executionContext: ExecutionContext) extends FutureRepository[Plant](stoner) {
  override implicit def alias: fragment.Fragment = fr"plants"

  @Inject()
  def this(stoner: DatabaseQueries)(implicit executionContext: ExecutionContext) {
    this(new FutureDatabaseQueries[Plant](stoner))
  }

  // TODO implement validation
  def exists(commonName: List[String], scientificName: List[String]): Future[Boolean] = Future(false)

  val += : PartialFunction[Plant, Future[Either[Message, Plant]]] = {

    case plant @ Plant(None, version, commonName, scientificName, description, metadata, hybrid, genus) =>

      (for {
        _ <- EitherT(this.exists(commonName, scientificName) map {
          case true => Left(new ConflictError[Plant].asInstanceOf[Message])
          // this is ignored
          case false => Right(plant)
        })
        insertedMovie <- EitherT(stoner.insertAndReturnID(insertQuery(version, commonName, scientificName, description, metadata, hybrid, genus)) map {
          case Left(e) => {
            e.printStackTrace()
            AddError(e.toString).asInstanceOf[Message].asLeft
          }
          case Right(id) => plant.copy(id = id).asRight
        })
      } yield insertedMovie).value
    case movie => addError(movie)
  }

  private def addError(plant: Plant) = Future(Left(new AddError(plant)))

  override def selectFrom(implicit alias: Fragment): Fragment = sql"select " ++ alias ++ sql".*, genera.name from " ++ alias ++ sql" left join genera on plants.genus_id = genera.id"

  override def tableName: String = "plants"
}

object PlantRepository {

  import spray.json._

  implicit val jsonMeta: Meta[PlantMetadata] =
    Meta.Advanced.other[PGobject]("json").timap[PlantMetadata](
      a => {
        PlantMessageFormat.read(a.getValue.parseJson)
      })(
        a => {
          val o = new PGobject
          o.setType("jsonb")
          o.setValue(a.toJson.compactPrint)
          o
        })

  def insertQuery(version: Int, commonName: List[String], scientificName: List[String], description: String, metadata: PlantMetadata, hybrid: Boolean, genus: Genus): Fragment = {
    val genusId = genus.id.getOrElse(GeneraService.UnknownId)
    sql"insert into plants (version, common_names, scientific_names, description, metadata, hybrid, genus_id) values ($version, $commonName, $scientificName, $description, $metadata, $hybrid, $genusId)"
  }

  implicit val plantReader: Read[Plant] = {
    Read[(Id, Version, List[String], List[String], String, PlantMetadata, Timestamp, Option[Timestamp], Boolean, Id, Option[String])].map {
      case (id, version, commonNames, scientificNames, description, metadata, _, _, hybrid, genusId, genusName) => {
        val genus = Genus(genusId, genusName.getOrElse(""))
        Plant(id, version, commonNames, scientificNames, description, metadata, hybrid, genus)
      }
    }
  }

  implicit val plantWriter: Write[Plant] =
    Write[(Id, Version, List[String], List[String], String, PlantMetadata, Boolean, Id)].contramap {
      plant => (plant.id, plant.version, plant.commonNames, plant.scientificNames, plant.description, plant.metadata, plant.hybrid, plant.genus.id)
    }

}
