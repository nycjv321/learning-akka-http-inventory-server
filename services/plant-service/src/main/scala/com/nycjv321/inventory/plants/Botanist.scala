package com.nycjv321.inventory.plants

import akka.actor.{Actor, ActorLogging, ActorSystem}
import akka.pattern.pipe
import com.nycjv321.inventory.plants.Botanist.ClassifyPlant

import scala.concurrent.ExecutionContext

object Botanist {
  final case class ClassifyPlant(plant: Plant)
  final case class UnClassifyPlant(id: Int)

  final case class GetPlant(id: Int)
  final case object GetPlants
}

class Botanist(system: ActorSystem, plants: PlantService)(implicit val executionContext: ExecutionContext) extends Actor with ActorLogging {
  override def receive: Receive = {
    case ClassifyPlant(plant) =>
      pipe(for {
        newPlant <- plants += plant
      } yield { newPlant }).to(sender())
    case Botanist.GetPlants =>
      pipe(plants.list).to(sender())
    case Botanist.GetPlant(id) =>
      pipe(plants.findById(id)).to(sender())
    case Botanist.UnClassifyPlant(id) => pipe(plants.deleteById(id)).to(sender())

  }
}