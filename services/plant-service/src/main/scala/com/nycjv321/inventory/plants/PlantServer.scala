package com.nycjv321.inventory.plants

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.google.inject.Injector
import com.nycjv321.inventory.template.MigratableInventoryServer

import scala.concurrent.ExecutionContext

object PlantServer extends App with MigratableInventoryServer {

  override def route(injector: Injector, materializer: ActorMaterializer)(implicit system: ActorSystem, executionContext: ExecutionContext): Route = {
    val plantService = injector.getInstance(classOf[PlantService])
    val botanistActor: ActorRef = system.actorOf(Props(new Botanist(system, plantService)), name = "botanist-actor")
    new BotanistRoute(system, botanistActor).routes
  }

  override def injector()(implicit system: ActorSystem) = Modules.asInjectable

  start()
}