package com.nycjv321.inventory.plants

import akka.actor.{ActorRef, ActorSystem}
import akka.event.Logging
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.MethodDirectives.{delete, get, post}
import akka.http.scaladsl.server.directives.PathDirectives.path
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.pattern.ask
import akka.util.Timeout
import PlantJsonSupport._
import com.nycjv321.inventory.{Message, NotFoundError}
import com.nycjv321.inventory.plants.Botanist.{ClassifyPlant, UnClassifyPlant}

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps

class BotanistRoute(system: ActorSystem, actorRef: ActorRef) {

  implicit val timeout: Timeout = Timeout(50000 milliseconds)

  lazy val log = Logging(system, classOf[BotanistRoute])
  lazy val routes: Route = pathPrefix("plants") {
    concat(
      pathEnd {
        concat(
          get {
            val plants: Future[Plants] = (actorRef ? Botanist.GetPlants).mapTo[Plants]
            onSuccess(plants) { resolvedPlants =>
              complete((StatusCodes.OK, resolvedPlants))
            }
          },
          post {
            entity(as[Plant]) { plant =>
              val newPlant: Future[Either[Message, Plant]] =
                (actorRef ? ClassifyPlant(plant)).mapTo[Either[Message, Plant]]
              onSuccess(newPlant) {
                case Left(message) => {
                  complete((StatusCodes.BadRequest, message))
                }
                case Right(value) => {
                  complete((StatusCodes.Created, value))
                }
              }
            }
          })
      },

      path(Segment) { (id) =>
        concat(
          get {
            val maybeUser: Future[Option[Plant]] =
              (actorRef ? Botanist.GetPlant(id.toInt)).mapTo[Option[Plant]]
            onSuccess(maybeUser) {
              case Some(plant) => {
                complete((StatusCodes.OK, plant))
              }
              case None => {
                complete((StatusCodes.NotFound, new NotFoundError[Plant](id.toInt).asInstanceOf[Message]))
              }
            }
          },
          delete {
            val userDeleted: Future[Either[Message, Plant]] =
              (actorRef ? UnClassifyPlant(id.toInt)).mapTo[Either[Message, Plant]]
            onSuccess(userDeleted) {
              case Left(value) => complete((StatusCodes.BadRequest, value))
              case Right(_) => complete((StatusCodes.OK, Message(s"deleted plant ($id")))
            }
          })
      })
  }

}