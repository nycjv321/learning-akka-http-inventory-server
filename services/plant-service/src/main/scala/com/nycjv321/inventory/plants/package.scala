package com.nycjv321.inventory

import java.time.LocalDate

package object plants {

  final case class Plant(override val id: Id = MissingId, override val version: Version = BaselineVersion, commonNames: List[String] = List.empty, scientificNames: List[String] = List.empty, description: String = "", metadata: PlantMetadata = PlantMetadata(), hybrid: Boolean = false, genus: Genus = Genus(name = "undefined")) extends Identifiable with Versioned {
    def commonName: String = commonNames.headOption.getOrElse("")
    def scientificName: String = scientificNames.headOption.getOrElse("")
  }

  final case class Genus(override val id: Id = MissingId, val name: String) extends Identifiable

  case class PlantMetadata(
                            lightConditions: List[String] = List.empty,
                            specialCharacteristics: List[String] = List.empty)


  final case class PurchasedPlant(override val id: Id = None, purchaseDate: LocalDate, plantId: Id) extends Identifiable

  final case class Plants(plants: Seq[Plant]) {
    def isEmpty: Boolean = this.plants.isEmpty
    def map[B](f: Plant => B): Seq[B] = plants.map(f)
  }

  sealed trait PlantError


}
