package com.nycjv321.inventory.plants

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import com.nycjv321.inventory.{Message, SnakifiedSprayJsonSupport}
import spray.json.{JsArray, JsBoolean, JsField, JsNumber, JsObject, JsString, JsValue, JsonFormat, RootJsonFormat, deserializationError}

import scala.util.Try

object PlantJsonSupport extends SprayJsonSupport with SnakifiedSprayJsonSupport {

  implicit val plantMetadataJsonFormat: RootJsonFormat[PlantMetadata] = jsonFormat2(PlantMetadata)

  implicit val genusJsonFormat: RootJsonFormat[Genus] = jsonFormat2(Genus)

  implicit val localDateFormat = new JsonFormat[LocalDate] {
    override def write(obj: LocalDate): JsValue = JsString(formatter.format(obj))

    override def read(json: JsValue): LocalDate = {
      json match {
        case JsString(lDString) =>
          Try(LocalDate.parse(lDString, formatter)).getOrElse(deserializationError(deserializationErrorMessage))
        case _ => deserializationError(deserializationErrorMessage)
      }
    }

    private val formatter = DateTimeFormatter.ISO_LOCAL_DATE
    private val deserializationErrorMessage =
      s"Expected date time in ISO offset date time format ex. ${LocalDate.now().format(formatter)}"
  }

  implicit object PlantMessageFormat extends RootJsonFormat[PlantMetadata] {
    override def read(json: JsValue): PlantMetadata = {
      val fields = json.asJsObject("{Invalid JSON Object}").fields
      val lightConditions = fields.get("light_conditions").fold(List.empty[String])(_.convertTo[List[String]])
      val specialCharacteristics = fields.get("special_characteristics").fold(List.empty[String])(_.convertTo[List[String]])
      PlantMetadata(lightConditions = lightConditions, specialCharacteristics = specialCharacteristics)
    }

    override def write(obj: PlantMetadata): JsValue = {
      plantMetadataJsonFormat.write(obj)
    }
  }

  implicit object PlantsFormat extends RootJsonFormat[Plants] {
    override def read(json: JsValue): Plants = {
      Plants(Seq.empty)
    }

    override def write(plants: Plants): JsValue = JsObject(("plants", JsArray(plants.map[JsValue](PlantFormat.write).toVector)))
  }

  implicit object PlantFormat extends RootJsonFormat[Plant] {
    override def read(json: JsValue): Plant = {
      val fields = json.asJsObject("{Invalid JSON Object}").fields

      if (fields.contains("message")) {
        throw new NoSuchElementException("not a plant")
      } else {
        val id = fields.get("id").fold(None.asInstanceOf[Option[Int]])(a => a.convertTo[Option[Int]])
        val version = fields.get("version").fold(0)(_.convertTo[Int])

        // we can check the version here and then manage what Plant type we return or vise a versa

        // TODO add logic to check for single form
        val commonNames = fields.get("common_names").fold(List.empty[String])(_.convertTo[List[String]])
        val scientificNames = fields.get("scientific_names").fold(List.empty[String])(_.convertTo[List[String]])
        val description = fields.get("description").fold("")(_.convertTo[String])
        val genus = GeneraService.find(fields.get("genus").fold("")(_.convertTo[String]))
        val hybrid = fields.get("hybrid").fold(false)(_.convertTo[Boolean])
        val metadata = fields.get("metadata").fold(PlantMetadata())(PlantMessageFormat.read)

        Plant(id, version, commonNames, scientificNames, description, metadata, hybrid, genus)
      }
    }

    override def write(plant: Plant): JsValue = {

      def commonName(plant: Plant): Option[JsField] = {
        plant.commonNames match {
          case Nil => None
          case head :: Nil => Some(("common_names", JsArray.apply(JsString(head))))
          case commonNames => Some(("common_names", JsArray.apply(commonNames.toVector.map(JsString.apply))))
        }
      }

      def scientificNames(plant: Plant): Option[JsField] = {
        plant.scientificNames match {
          case Nil => None
          case head :: Nil => Some(("scientific_names", JsArray.apply(JsString(head))))
          case scientificNames => Some(("scientific_names", JsArray.apply(scientificNames.toVector.map(JsString.apply))))
        }
      }

      def description(plant: Plant): Option[JsField] = {
        plant.description match {
          case "" => None
          case description => Some("description", JsString(description))
        }
      }

      def genus(plant: Plant): Option[JsField] = {
        plant.genus match {
          case Genus(id, name) if id.contains(GeneraService.UnknownId) || name.isEmpty => None
          case Genus(_, name) => Some("genus", JsString(name))
        }
      }

      val optionalFields: List[JsField] = List(commonName(plant), scientificNames(plant), description(plant), genus(plant)).flatten
      val fields: List[JsField] = List(
        ("id", JsNumber.apply(plant.id.getOrElse(0))),
        ("version", JsNumber.apply(plant.version)),
        ("hybrid", JsBoolean.apply(plant.hybrid))) ++ optionalFields

      JsObject(
        fields: _*)
    }
  }

}
