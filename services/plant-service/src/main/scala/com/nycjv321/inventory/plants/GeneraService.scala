package com.nycjv321.inventory.plants

object GeneraService {

  val UnknownId = 0
  val Unknown = Genus(Some(UnknownId), "Unknown")
  val Sedum = Genus(Some(1), "Sedum")
  val Echeveria = Genus(Some(2), "Echeveria")
  val Kalanchoe = Genus(Some(3), "Kalanchoe")
  val Cactaceae = Genus(Some(4), "Cactaceae")

  private def list : Seq[Genus] = Seq(Sedum, Echeveria, Kalanchoe, Cactaceae)

  def find(name: String): Genus = list.find(_.name == name).getOrElse(Unknown)

}
