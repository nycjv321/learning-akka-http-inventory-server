package com.nycjv321.inventory.plants

import cats.implicits._
import com.nycjv321.inventory.Message
import javax.inject.Inject

import scala.concurrent.{ExecutionContext, Future}
class PlantService @Inject() (plants: PlantRepository)(implicit val executionContext: ExecutionContext) {
  def deleteById(id: Int): Future[Either[Message, Int]] = {
    plants.deleteById(id).map {
      case Left(t) => Message(t.getMessage).asLeft
      case Right(t) => t.asRight
    }
  }

  def +=(plant: Plant): Future[Either[Message, Plant]] = {
    plants += plant
  }

  def list: Future[Plants] = plants.list().map(Plants.apply)

  def findById(id: Int): Future[Option[Plant]] = plants.findById(id)

}
