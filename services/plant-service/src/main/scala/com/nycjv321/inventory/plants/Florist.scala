package com.nycjv321.inventory.plants

object Florist {
  final case class PurchasePlant(purchasePlant: PurchasedPlant)
  final case class LookupPurchase(purchasePlant: PurchasedPlant)
  final case class ReturnPlant(purchasePlant: PurchasedPlant)
  final case object GetPlants
}
