create table if not exists genera (
    id             serial   primary key,
    name           varchar(30) default '' not null ,
    created          timestamp with time zone default now() not null,
    modified         timestamp with time zone
);

ALTER TABLE genera ADD CONSTRAINT genera_unique_name UNIQUE (name);

INSERT INTO genera (id, name) VALUES (0, 'Unknown') ON CONFLICT ON CONSTRAINT genera_unique_name DO NOTHING;
INSERT INTO genera (id, name) VALUES (1, 'Sedum') ON CONFLICT ON CONSTRAINT genera_unique_name DO NOTHING;
INSERT INTO genera (id, name) VALUES (2, 'Echeveria') ON CONFLICT ON CONSTRAINT genera_unique_name DO NOTHING;
INSERT INTO genera (id, name) VALUES (3, 'Kalanchoe') ON CONFLICT ON CONSTRAINT genera_unique_name DO NOTHING;
INSERT INTO genera (id, name) VALUES (4, 'Cactaceae') ON CONFLICT ON CONSTRAINT genera_unique_name DO NOTHING;




create table if not exists plants (
    id               serial   primary key,
    version          int not null,
    common_names     varchar(300)[],
    scientific_names varchar(300)[],
    description      text,
    hybrid           boolean default false not null,
    metadata         jsonb default '{}' NOT NULL,
    created          timestamp with time zone default now() not null,
    modified         timestamp with time zone,
    genus_id         serial REFERENCES genera (id)
);

create function update_modified_column() returns trigger
    language plpgsql
as
$$
BEGIN
    NEW.modified = now();
    RETURN NEW;
END;
$$;

CREATE TRIGGER update_plants_modified_time BEFORE UPDATE ON plants FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();
CREATE TRIGGER update_genera_modified_time BEFORE UPDATE ON genera FOR EACH ROW EXECUTE PROCEDURE  update_modified_column();
