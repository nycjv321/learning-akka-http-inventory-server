# Playground for learning Akka-Http


## Scala Formatter

    sbt scalafmtAll


## Services

### File Service

This functions as a basic file storage service

* Barely functional
* Compiles
* Includes basic test

### Movie Service

This service functions as a movie catalog 

* Minimally functional
* Compiles
* Includes Tests

### Game Service

This service functions as a game collection service 


* Minimally functional
* Compiles
* Includes Tests

### Plant Service

This service functions as a plant collection service

* Minimally functional
* There are compilation issues
* Includes Tests
