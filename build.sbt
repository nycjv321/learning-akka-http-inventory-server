lazy val akkaHttpVersion = "10.1.9"
lazy val akkaVersion = "2.6.0-M5"

scalacOptions += "-Ypartial-unification" // 2.11.9+

lazy val doobieVersion = "0.8.8"

lazy val core = project
  .settings(
    libraryDependencies ++= Seq(
      "net.codingwell" %% "scala-guice" % "4.2.6",
      "com.github.pureconfig" %% "pureconfig" % "0.11.1",
      "com.chuusai" %% "shapeless" % "2.3.3",
      "org.flywaydb" % "flyway-core" % "6.0.8",
      "org.typelevel" %% "cats-core" % "2.0.0-M4",
      "org.typelevel" %% "cats-effect" % "2.0.0-M4",
      "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-http-xml" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,
      "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
      "ch.qos.logback" % "logback-classic" % "1.2.3"
    )
  )

lazy val test = project
  .dependsOn(core)
  .settings(
    libraryDependencies ++= Seq(
      "net.codingwell" %% "scala-guice" % "4.2.6",
      "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion,
      "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
      "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion,
      "org.scalatest" %% "scalatest" % "3.1.0"
    )
  )

lazy val database = project
  .dependsOn(core)
  .settings(
    libraryDependencies ++= Seq(
      "org.postgresql" % "postgresql" % "42.2.6",
      "org.tpolecat" %% "doobie-core" % doobieVersion,
      "org.tpolecat" %% "doobie-postgres" % doobieVersion,
      "org.tpolecat" %% "doobie-hikari" % doobieVersion,
      "org.tpolecat" %% "doobie-specs2" % doobieVersion,
      "io.getquill" %% "quill-async-postgres" % "3.5.0"

    )
  )

lazy val template = (project in file("services/template"))
  .dependsOn(core, database, test % "test")

lazy val movies =
  (project in file("services/movie-service")).dependsOn(template, test % "test")

lazy val plants =
  (project in file("services/plant-service")).dependsOn(template, test % "test")

lazy val files =
  (project in file("services/file-service")).dependsOn(template, test % "test")

lazy val games =
  (project in file("services/games-service")).dependsOn(template, test % "test")

// see https://www.scala-sbt.org/1.x/docs/Multi-Project.html

lazy val root = (project in file("."))
  .aggregate(database, core, database, movies, files, template, games)
  .settings(
    inThisBuild(
      List(
        organization := "com.nycjv321",
        scalaVersion := "2.12.8"
      )
    ),
    name := "inventory",
    libraryDependencies ++= Seq(
    )
  )
